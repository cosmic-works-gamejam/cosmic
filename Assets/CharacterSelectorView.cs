﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public enum CharacterName
{
    Kyle,
    Lilith,
    name3,
    NONE
}

public class CharacterSelectorView : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _buttonCharacterOneText,
        _buttonCharacterTwoText,_titleCharactersText,
        _buttonCharacterThreeText,
        _buttonBackText, _titleCharacterOneText, _titleCharacterTwoText, _titleCharacterThreeText;

    [SerializeField] private Button _buttonCharacterOne, _buttonCharacterTwo, _buttonCharacterThree, _buttonBack;

    public event Action<CharacterName> OnCharacterOneSelected, OnCharacterTwoSelected, OnCharacterThreeSelected;
    public event Action OnButtonBackSelected;

    void Start()
    {
        _buttonCharacterOne.onClick.AddListener(() => OnCharacterOneSelected.Invoke(CharacterName.Kyle));
        _buttonCharacterTwo.onClick.AddListener(() => OnCharacterTwoSelected.Invoke(CharacterName.Lilith));
        _buttonCharacterThree.onClick.AddListener(() => OnCharacterThreeSelected.Invoke(CharacterName.name3));
        
        _buttonBack.onClick.AddListener(() => OnButtonBackSelected.Invoke());
    }

    private void OnDestroy()
    {
        _buttonCharacterOne.onClick.RemoveListener(() => OnCharacterOneSelected.Invoke(CharacterName.Kyle));
        _buttonCharacterTwo.onClick.RemoveListener(() => OnCharacterTwoSelected.Invoke(CharacterName.Lilith));
        _buttonCharacterThree.onClick.RemoveListener(() => OnCharacterThreeSelected.Invoke(CharacterName.name3));
        
        _buttonBack.onClick.RemoveListener(() => OnButtonBackSelected.Invoke());
    }

    public void SetLanguageStrings(LanguageText languageText)
    {
        _titleCharacterOneText.SetText(CharacterName.Kyle.ToString());
        _titleCharacterTwoText.SetText(CharacterName.Lilith.ToString());
        _titleCharacterThreeText.SetText(CharacterName.name3.ToString());
        
        _buttonCharacterOneText.SetText($"{languageText.SelectCharacterToPlayInitScene} {CharacterName.Kyle.ToString()}") ;
        _buttonCharacterTwoText.SetText($"{languageText.SelectCharacterToPlayInitScene} {CharacterName.Lilith.ToString()}") ;
        _buttonCharacterThreeText.SetText($"{languageText.SelectCharacterToPlayInitScene} {CharacterName.name3.ToString()}") ;
        _titleCharactersText.SetText(languageText.CharactersTitleInitScene);
        _buttonBackText.SetText(languageText.BackButtonInitScene);
    }
}