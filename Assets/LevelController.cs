using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Utils;
using Utils.Input;

public class LevelController : MonoBehaviour
{
    private ReadInputPlayer _readInputPlayer;
    private SceneChanger _sceneChanger;
   [SerializeField] private CanvasFader _canvasFader;
   // [SerializeField] private ChangeSceneEvent _changeSceneEvent;

    private ISceneModel _sSceneModel;

    private AsyncOperation operationLoadingScene;

    // Start is called before the first frame update
    void Start()
    {
        _readInputPlayer = ServiceLocator.Instance.GetService<ReadInputPlayer>();
        _sSceneModel = ServiceLocator.Instance.GetModel<ISceneModel>();
        _sceneChanger = ServiceLocator.Instance.GetService<SceneChanger>();
        _sSceneModel.PreviousScene = _sceneChanger.GetCurrentSceneName();
        _sSceneModel.NextScene = "SampleScene";
        _readInputPlayer.OnPlayerPressedControlKey += ControlPressed;
        _canvasFader.OnFadeCompleted += InitializeLoadingScene;
    }

    private void InitializeLoadingScene()
    {
        operationLoadingScene.allowSceneActivation = true;
    }

    private void ControlPressed()
    {
        Debug.Log("Pressed F");
        _canvasFader.ActivateFader();
        operationLoadingScene = SceneManager.LoadSceneAsync(_sSceneModel.LoadingScene, LoadSceneMode.Single);
        operationLoadingScene.allowSceneActivation = false;
        operationLoadingScene.completed += SceneLoaded;
    }

    private void SceneLoaded(AsyncOperation obj)
    {
        operationLoadingScene.completed -= SceneLoaded;
        Debug.Log("Completed LoadingScene");
    }
}