﻿using Extensions.UnityEngine;
using UnityEngine;
using UnityEngine.Audio;
using Utils;

public class AudioManagerBase : MonoBehaviour
{
    [SerializeField] private AudioMixer _audioMixer;
    private IAudioModel _audioModel;
    private bool _volumeMasterStatusAtStart, _volumeEffectsStatusAtStart, _volumeSoundTrackVolumeStatusAtStart;
    private float _volumeMasterValueAtStart, _volumeEffectsValueAtStart, _backgroundVolumeValueAtStart;
    private bool _volumeMasterCurrent, _volumeEffectsCurrent, _volumeSoundTrackVolumeCurrent;

    private void Awake()
    {
        _audioModel = ServiceLocator.Instance.GetModel<IAudioModel>();
    }

    private void Start()
    {
        _volumeMasterStatusAtStart = _audioModel.MasterVolumeStatus;
        _volumeEffectsStatusAtStart = _audioModel.VFXVolumeStatus;
        _volumeSoundTrackVolumeStatusAtStart = _audioModel.SoundTrackVolumeStatus;
        
        _volumeMasterValueAtStart = 100;
        _volumeEffectsValueAtStart = 100;
        _backgroundVolumeValueAtStart = 100;
        UpdateMasterVolume(_volumeMasterStatusAtStart);
        UpdateVFXVolume(_volumeEffectsStatusAtStart);
        UpdateBackgroundVolume(_volumeSoundTrackVolumeStatusAtStart);
    }

    public void UpdateVFXVolume(bool vfxVolumeStatus)
    {
        _audioModel.VFXVolumeStatus = vfxVolumeStatus;
        _audioMixer.SetFloatAsLinear("VFXVolume", vfxVolumeStatus ? _volumeEffectsValueAtStart : 0.0f);
    }

    public void UpdateBackgroundVolume(bool soundtrackVolumeStatus)
    {
        _audioModel.SoundTrackVolumeStatus = soundtrackVolumeStatus;
        _audioMixer.SetFloatAsLinear("BackgroundVolume",
            soundtrackVolumeStatus ? _backgroundVolumeValueAtStart : 0.0f);
    }

    public void UpdateMasterVolume(bool masterVolumeStatus)
    {
        _audioModel.MasterVolumeStatus = masterVolumeStatus;
        _audioMixer.SetFloatAsLinear("MasterVolume", masterVolumeStatus ? _volumeMasterValueAtStart : 0.0f);
    }
}