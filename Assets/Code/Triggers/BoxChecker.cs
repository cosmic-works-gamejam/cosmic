﻿using UnityEngine;

public class BoxChecker : MonoBehaviour
{
    private bool _hasExecutedBefore;

    private void Start()
    {
        _hasExecutedBefore = false;
    }

    public void OnTriggerStay(Collider other)
    {
        if (_hasExecutedBefore) return;

        var cube = other.GetComponent<Cube>();
        if (!cube) return;
        Debug.Log("INTERACT " + cube.PlayerIsInteractingWithIt());
        if (cube.PlayerIsInteractingWithIt()) return;
        var executeAction = cube.GetComponent<IExecuteAction>();
        executeAction?.ExecuteAction();
        _hasExecutedBefore = true;
    }

    private void OnTriggerExit(Collider other)
    {
        OnTriggerStay(other);
    }
}