﻿using UnityEditor;
using UnityEngine;
#if UNITY_EDITOR
[CustomEditor(typeof(FieldOfView3D))]
public class FieldOfViewEditor : Editor
{
    private Vector3 _transformPosition;

    private void OnSceneGUI()
    {
        FieldOfView3D fieldOfView = (FieldOfView3D) target;
        Handles.color = Color.white;
        _transformPosition = fieldOfView.transform.position;
        Handles.DrawWireArc(_transformPosition, Vector3.up, Vector3.forward, 360, fieldOfView.viewRadius);
        Vector3 viewAngleA = fieldOfView.DirectionFromAngle(-fieldOfView.viewAngle / 2, false);
        Vector3 viewAngleB = fieldOfView.DirectionFromAngle(fieldOfView.viewAngle / 2, false);
        Handles.DrawLine(_transformPosition,
            _transformPosition + viewAngleA * fieldOfView.viewRadius);
        Handles.DrawLine(_transformPosition,
            _transformPosition + viewAngleB * fieldOfView.viewRadius);
        
        Handles.color = Color.red;
        foreach (var VARIABLE in fieldOfView.visibleTargets)
        {
            Handles.DrawLine(_transformPosition, VARIABLE.transform.position);
        }
    }
}
#endif