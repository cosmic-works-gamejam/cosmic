﻿using System;
using System.Collections;
using UnityEngine;

public class Timer : IEnumerator
{
    public event Action OnTimerEnds = delegate { };
    private float _timeToWait;

    public void SetTimeToWait(float time)
    {
        _timeToWait = time;
    }

    public IEnumerator TimerCoroutine()
    {
        yield return new WaitForSeconds(_timeToWait);
        OnTimerEnds.Invoke();
    }

    public bool MoveNext()
    {
        throw new NotImplementedException();
    }

    public void Reset()
    {
        throw new NotImplementedException();
    }

    public object Current { get; }
}