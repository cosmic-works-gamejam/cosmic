﻿using UnityEngine;

public interface IPunchable
{
    void Punch(Transform objectWhichHit);
}