﻿using App.PlayerModelInfo;
using Signals.EventManager;
using UnityEngine;
using Utils;
using Utils.Input;

namespace Presentation.Player
{
    public class PlayerFacade : CharacterFacade
    {
        [SerializeField] private ItemInteractor _itemInteractor;
        [SerializeField] private PlayerMovement _playerMovement;
        [SerializeField] private Animator _animator;

        private IPlayerModel _playerModel;
        private IEventManager _eventManager;
        private ReadInputPlayer _readInputPlayer;
        private bool _leftMouseClicked;
        public IPlayerModel PlayerModel => _playerModel;

        void Awake()
        {
            _playerModel = ServiceLocator.Instance.GetModel<IPlayerModel>();
            _eventManager = ServiceLocator.Instance.GetService<IEventManager>();
            _readInputPlayer = ServiceLocator.Instance.GetService<ReadInputPlayer>();
            _playerMovement.Init(_readInputPlayer);
            //TODO UTILIZAR FORWARD DEL PLAYER MAS QUE LA CAMARA
            // _itemInteractor.Init(_camera, _playerModel);
        }


        private void Start()
        {
            _readInputPlayer.OnPlayerPressLeftButtonMouse += ClickLeftMouseButton;
            _readInputPlayer.OnPlayerReleaseLeftButtonMouse += ReleaseLeftMouseButton;
            _eventManager.AddActionToSignal<PlayerMustWaitToGetCubeAgainSignal>(Wait);
        }


        private void Wait(PlayerMustWaitToGetCubeAgainSignal obj)
        {
            _itemInteractor.WaitToInteractAgain();
        }

        private void ReleaseLeftMouseButton()
        {
            // Debug.Log("Soltamos Left Mouse");
            _leftMouseClicked = false;
            if(_itemInteractor.IsInteracting)
                new ResetEnemiesPathSignal().Execute();
            
            _itemInteractor.StopInteractionWithItem();
        }

        private void OnDestroy()
        {
            _readInputPlayer.OnPlayerPressLeftButtonMouse -= ClickLeftMouseButton;
            _readInputPlayer.OnPlayerReleaseLeftButtonMouse -= ReleaseLeftMouseButton;
            _eventManager.RemoveActionFromSignal<PlayerMustWaitToGetCubeAgainSignal>(Wait);
        }

        private void ClickLeftMouseButton()
        {
            _leftMouseClicked = true;

            _itemInteractor.StartInteractionWithItem();
        }

        public override Vector3 VectorOfMovement()
        {
            throw new System.NotImplementedException();
        }

        public override Animator GetAnimator()
        {
            return _animator;
        }
    }
}