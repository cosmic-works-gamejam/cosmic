﻿using System;

namespace Presentation.Player
{
    using UnityEngine;
    using Utils.Input;


    public class PlayerMovement : MonoBehaviour
    {
        private ReadInputPlayer _readInputPlayer;
        [SerializeField] private float _standingMoveSpeed, _crouchMoveSpeed, _jumpForce, _rotationSpeed;
        [SerializeField] private Rigidbody _rigidbody;
        [SerializeField] private GroundChecker _groundChecker;
        [SerializeField] private Animator _animator;
        [SerializeField] private PlayerColliders playerColliders;
        [SerializeField] private WallCollisionChecker wallCollisionChecker;
        private float _currentSpeed, _colliderOriginalSize;
        private bool _jumpPressed, _canMove, _crotch, _canMoveForward;
        private Vector3 _jumpMovement;
        private static readonly int MovementZ = Animator.StringToHash("MovementZ");
        private static readonly int MovementX = Animator.StringToHash("MovementX");
        private static readonly int Crouch = Animator.StringToHash("Crouch");
        private static readonly int Speed = Animator.StringToHash("Speed");

        private Vector3 input;

        private Quaternion _rotation;
        // public Vector3 MovementDirection => _movementDirection;
        // public Quaternion GetRotation => _rotation;

        void Start()
        {
            Cursor.visible = false;
            _canMove = true;
            _currentSpeed = _standingMoveSpeed;
            wallCollisionChecker.OnWallAhead += StopMovement;
            wallCollisionChecker.OnNotWall += ResumeMovement;
        }
        
  
        private void ResumeMovement()
        {
            _canMoveForward = true;
        }

        private void StopMovement()
        {
            _canMoveForward = false;
        }

        public void Init(ReadInputPlayer readInputPlayer)
        {
            _readInputPlayer = readInputPlayer;
            _readInputPlayer.OnPlayerPressedSpaceKey += EnableJump;
            _readInputPlayer.OnPlayerPressedControlKey += EnableCrouch;
            _readInputPlayer.OnPlayerReleaseControlKey += DisableCrouch;
        }

        private void DisableCrouch()
        {
            _crotch = false;
            _currentSpeed = _standingMoveSpeed;
            playerColliders.DisableCrouchCollider();
        }

        private void EnableCrouch()
        {
            _crotch = true;
            _currentSpeed = _crouchMoveSpeed;
            playerColliders.EnableCrouchCollider();
        }

        private void EnableJump()
        {
            if (!_groundChecker.IsGround())
            {
                return;
            }

            _jumpPressed = true;
        }

        private void Update()
        {
            input = _readInputPlayer.MovementInGameAxis;
        }

        void FixedUpdate()
        {
            if (!_canMove)
            {
                return;
            }

            // Debug.Log($"INPUT {input.x} - {input.z} {_crotch} {_jumpPressed}");
            _jumpMovement = Vector3.zero;

            if (_jumpPressed && !_crotch)
            {
                // Debug.Log($"JUMP{_jumpPressed}");
                _jumpMovement = Vector3.up * _jumpForce;
                _jumpPressed = false;
                _rigidbody.AddForce(_jumpMovement, ForceMode.Force);
                new PlayPlayerJumpSoundSignal().Execute();
            }

            if (!_jumpPressed && _crotch)
            {
                // Debug.Log("CROTCH");
                _animator.SetBool(Crouch, true);
            }

            if (!_crotch)
            {
                _animator.SetBool(Crouch, false);
            }

            _animator.SetFloat(MovementZ, input.z);
            _animator.SetFloat(Speed, input.magnitude);
            // _animator.SetFloat(MovementX, input.x);
            _rotation = Quaternion.Euler(_rigidbody.rotation.eulerAngles + Vector3.up * input.x * _rotationSpeed);
            _rigidbody.rotation = _rotation;
            
            
            Vector3 movement = transform.forward * input.z;
            if(!_canMoveForward && input.z >0.01f) return;
            wallCollisionChecker.Direction = movement;
            // movement.y = 0;
            _rigidbody.position += movement * Time.deltaTime * _currentSpeed;
        }
    }
}