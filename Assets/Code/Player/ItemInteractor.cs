﻿using System;
using Presentation.Items;
using Signals.EventManager;
using UnityEngine;
using Utils;

namespace Presentation.Player
{
    public class ItemInteractor : MonoBehaviour
    {
        [SerializeField] private FieldOfView3D _fieldOfView3D;

        // [SerializeField] private PlayerMovement _playerMovement;
        private PickableItem _pickableItemInfFrontOfPlayer;
        private Transform _itemPlayerInteraction;
        private bool _stopInteracting = false, _isInteracting, _canInteract = true;

        private IEventManager _eventManager;
        private Timer _timer;

        public bool IsInteracting => _isInteracting;

        private void Awake()
        {
            _eventManager = ServiceLocator.Instance.GetService<IEventManager>();
        }



        private void Start()
        {
            _fieldOfView3D.OnResetHitItem += ResetItemToPickFromFOV;
            _fieldOfView3D.OnHitSomething += HasHitSomething;
            _timer = new Timer();
            _timer.SetTimeToWait(5);
        }

        private void ResetItemToPickFromFOV()
        {
            _itemPlayerInteraction = null;
            // Debug.Log($"{_pickableItemInfFrontOfPlayer} {_isInteracting}");
            if (_pickableItemInfFrontOfPlayer == null || IsInteracting) return;
            _pickableItemInfFrontOfPlayer.StopInteraction();
            _pickableItemInfFrontOfPlayer = null;
        }


        private void FixedUpdate()
        {
            if (!IsInteracting || !_canInteract) return;
            if (!_pickableItemInfFrontOfPlayer)
            {
                return;
            }

            _pickableItemInfFrontOfPlayer.InteractWithObject(gameObject);
        }

        private bool PlayerIsInteractingWithItem()
        {
            return _pickableItemInfFrontOfPlayer != null || _itemPlayerInteraction != null;
        }


        private void ResetItemToPick()
        {
            if (!PlayerIsInteractingWithItem()) return;
            _itemPlayerInteraction = null;
            if (!_pickableItemInfFrontOfPlayer) return;
            _pickableItemInfFrontOfPlayer.StopInteraction();
            _pickableItemInfFrontOfPlayer = null;
        }


        public void StartInteractionWithItem()
        {
            if (!_itemPlayerInteraction) return;

            var enemy = _itemPlayerInteraction.GetComponent<IPunchable>();
            if (enemy != null)
            {
                enemy.Punch(transform);
                return;
            }

            // var itemToInteract = _itemPlayerInteraction.GetComponent<Doo>();
            // if (itemToInteract != null)
            // {
            //     itemToInteract.InteractWithObject();
            //     return;
            // }

            _isInteracting = true;
        }

        public void StopInteractionWithItem()
        {
            _isInteracting = false;

            ResetItemToPick();
            // ResetItemToPick();
        }


        private void HasHitSomething(Transform hitCollider)
        {
            // Debug.Log($"HIT {hitCollider}");
            _itemPlayerInteraction = hitCollider;

            bool PickableItemIsInFrontOfPlayer(Transform raycastHit1)
            {
                return raycastHit1.gameObject.layer == LayerMask.NameToLayer("Pickable");
            }

            bool InteractableItemIsInFrontOfPlayer(Transform raycastHit1)
            {
                return raycastHit1.gameObject.layer == LayerMask.NameToLayer("Interactable");
            }

            if (PickableItemIsInFrontOfPlayer(hitCollider) && !_pickableItemInfFrontOfPlayer)
            {
                _pickableItemInfFrontOfPlayer = hitCollider.GetComponent<PickableItem>();
            }

            if (InteractableItemIsInFrontOfPlayer(hitCollider) && !_pickableItemInfFrontOfPlayer)
            {
                _itemPlayerInteraction = hitCollider;
            }
        }

        public void WaitToInteractAgain()
        {
            _canInteract = false;
            _timer.OnTimerEnds += EnableInteractionAgain;
            StartCoroutine(_timer.TimerCoroutine());
        }

        private void EnableInteractionAgain()
        {
            _timer.OnTimerEnds -= EnableInteractionAgain;
            _canInteract = true;
        }
    }
}