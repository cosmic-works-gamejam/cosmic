﻿namespace App.PlayerModelInfo
{
    public interface IPlayerModel
    {
        CharacterName CharacterNameSelected { get; set; }

        void ResetData();
    }
}