﻿using App.PlayerModelInfo;
using Domain.JsonTranslator;
using Domain.LoaderSaveGame;
using Domain.SaverGame;
using Signals.EventManager;
using UnityEngine;
using Utils;
using Utils.Input;

namespace Presentation.Installers
{
    public class InstallerInGamePresentation : MonoBehaviour
    {
        private GameObject _readInputPlayerPrefab,
            _languageManagerPrefab,
            _updateProgressPlayerPrefab, _retrieveProgressPlayerPrefab,
            _sceneChangerManagerPrefab, _objectVFXPrefab, _audioManagerInGame;

        private GameObject _readInputPlayerInstance,
            _languageManagerInstance,
            _updateProgressPlayerInstance, _retrieveProgressPlayerInstance,
            _sceneChangerManagerInstance, _objectVFXInstance, _audioManagerInstance;

        private void Awake()
        {
            //TODO USAR ADDRESABLES
            _readInputPlayerPrefab = Resources.Load("Prefabs/Managers/ReadInputPlayer") as GameObject;
            _languageManagerPrefab = Resources.Load("Prefabs/Managers/LanguageManager") as GameObject;
            _updateProgressPlayerPrefab = Resources.Load("Prefabs/Managers/UpdatePlayerProgression") as GameObject;
            _retrieveProgressPlayerPrefab = Resources.Load("Prefabs/Managers/RetrievePlayerProgression") as GameObject;
            _sceneChangerManagerPrefab = Resources.Load("Prefabs/Managers/SceneChangerManager") as GameObject;
            _objectVFXPrefab = Resources.Load("Prefabs/Managers/VFX/Object VFX") as GameObject;
            _audioManagerInGame = Resources.Load("Prefabs/Managers/AudioManagerInGameScene") as GameObject;
            
            ServiceLocator.Instance.RegisterModel<IPlayerModel>(new PlayerModel());
            ServiceLocator.Instance.RegisterModel<IAudioModel>(new AudioModel());
            ServiceLocator.Instance.RegisterModel<ISceneModel>(new SceneModel());
            ServiceLocator.Instance.RegisterService<IEventManager>(new EventManager());
            ServiceLocator.Instance.RegisterService<IJsonator>(new JsonUtililyTransformer());
            ServiceLocator.Instance.RegisterService<ISaver>(new SaveUsingPlayerPrefs());
            ServiceLocator.Instance.RegisterService<ILoader>(new LoadWithPlayerPrefs());
            
            
            _readInputPlayerInstance = Instantiate(_readInputPlayerPrefab, transform);
            _languageManagerInstance = Instantiate(_languageManagerPrefab,transform);
            _updateProgressPlayerInstance = Instantiate(_updateProgressPlayerPrefab,transform);
            _sceneChangerManagerInstance = Instantiate(_sceneChangerManagerPrefab,transform);
            _retrieveProgressPlayerInstance = Instantiate(_retrieveProgressPlayerPrefab,transform);
            _objectVFXInstance = Instantiate(_objectVFXPrefab,transform);
            // _audioManagerInstance = Instantiate(_audioManagerInGame,transform);

            ServiceLocator.Instance.RegisterService(_languageManagerInstance
                .GetComponent<ILanguageManager>());
            ServiceLocator.Instance.RegisterService(_readInputPlayerInstance.GetComponent<ReadInputPlayer>());
            ServiceLocator.Instance.RegisterService(_sceneChangerManagerInstance.GetComponent<SceneChanger>());
            ServiceLocator.Instance.RegisterService(_updateProgressPlayerInstance.GetComponent<UpdatePlayerProgression>());
            ServiceLocator.Instance.RegisterService(_retrieveProgressPlayerInstance.GetComponent<RetrievePlayerProgression>());
            
            _languageManagerInstance
                .GetComponent<ILanguageManager>().SetActualLanguageText(LanguagesKeys.SPA);

            // DontDestroyOnLoad(this);
        }

        private void OnDestroy()
        {
            ServiceLocator.Instance.UnregisterService<ReadInputPlayer>();
            ServiceLocator.Instance.UnregisterService<IEventManager>();
            ServiceLocator.Instance.UnregisterService<ILanguageManager>();
        }
    }
}