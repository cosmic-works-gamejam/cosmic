﻿using UnityEngine;

namespace Presentation.Items
{
    public abstract class PickableItem : MonoBehaviour
    {
        public abstract void InteractWithObject(GameObject objectWhichInteract);
        public abstract void StopInteraction();
        public abstract bool PlayerIsInteractingWithIt();
    }
}