﻿// using DG.Tweening;
// using UnityEngine;
//
// namespace VFX
// {
//     public class DissolveObject : MonoBehaviour
//     {
//         [SerializeField] private ParticleSystem ps = default;
//         [SerializeField] private float dissolveTime = 1f;
//         [SerializeField] private Material dissolveMaterial, normalMaterial;
//         private Material _currentMaterial;
//         private float dissolveAmount = 0f;
//         private float velocity;
//
//         private bool _startDissolve;
//
//         private void Awake()
//         {
//             _currentMaterial = normalMaterial;
//             _startDissolve = false;
//         }
//
//         private void Start()
//         {
//             ParticleSystem.MainModule psm = ps.main;
//
//             psm.duration = dissolveTime;
//             ps.Play();
//         }
//
//         private void Update()
//         {
//             if (!_startDissolve) return;
//             dissolveAmount = Mathf.SmoothDamp(dissolveAmount, 1, ref velocity, dissolveTime);
//
//             // Valor entre 0 y 1
//             _currentMaterial.SetFloat("dissolveAmount", dissolveAmount);
//         }
//
//         public void StartDissolve()
//         {
//             _startDissolve = true;
//             _currentMaterial = dissolveMaterial;
//             GetComponent<Renderer>().material = dissolveMaterial;
//             ObjectEffect.Singleton.OneTimeEffect(_renderer, ObjectEffect.Singleton.Dissolve);
//
//         }
//     }
// }