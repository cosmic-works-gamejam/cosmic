﻿using UnityEngine;

namespace VFX
{
    [System.Serializable]
    public class Effect
    {
        [SerializeField] private float targetValue = 0f;
        [SerializeField] private Material effectMaterial = default;
        [SerializeField] private string shaderPropertyName = "";

        public float TargetValue
        {
            get
            {
                return targetValue;
            }
        }

        // public float EffectTime
        // {
        //     get
        //     {
        //         return effectTime;
        //     }
        // }

        public Material EffectMaterial
        {
            get
            {
                return effectMaterial;
            }
        }

        public string ShaderPropertyName
        {
            get
            {
                return shaderPropertyName;
            }
        }
    }
}
