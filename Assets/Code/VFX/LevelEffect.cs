﻿using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

namespace VFX
{
    public class LevelEffect : MonoBehaviour
    {
        public static LevelEffect Singleton;

        [SerializeField] private float _timeToDissolveObjects = 2.0f;
        [SerializeField] private List<Renderer> _objectsToDissolveAtEnd;

        [SerializeField] private GameObject _player;
        // [SerializeField] private SkinnedMeshRenderer _playerRenderer;

        [Header("Level Effects")] [SerializeField]
        private Effect changeColor = default;

        [SerializeField] private Effect openDoor = default;
        [SerializeField] private Effect dissolve = default;


        private void Awake()
        {
            if (!Singleton)
            {
                Singleton = this;
            }

            else
            {
                Destroy(gameObject);
                return;
            }

            // levelObjectRenders = renderLevelObjects.GetComponentsInChildren<Renderer>();
        }

        private void Start()
        {
            //OneTimeEffect(openDoor).OnComplete(() => { OneTimeEffect(dissolve); });
        }

        public Sequence OneTimeEffect(Renderer objectRenderer, Effect effect, float timeToExecuteEffect)
        {
            Sequence sequence = DOTween.Sequence();

            objectRenderer.material = effect.EffectMaterial;
            sequence.Join(objectRenderer.material.DOFloat(effect.TargetValue, effect.ShaderPropertyName,
                timeToExecuteEffect));

            return sequence;
        }

        public Sequence DissolveLevelEffect()
        {
            Sequence sequence = DOTween.Sequence();
            _player.gameObject.SetActive(false);
            foreach (Renderer objectRenderer in _objectsToDissolveAtEnd)
            {
                if(!objectRenderer) continue;
                objectRenderer.material = DissolveLevel.EffectMaterial;
                sequence.Join(objectRenderer.material.DOFloat(DissolveLevel.TargetValue,
                    DissolveLevel.ShaderPropertyName, _timeToDissolveObjects));
            }

            return sequence;
        }

        public Effect DissolveLevel
        {
            get { return dissolve; }
        }

        public Effect OpenDoor
        {
            get { return openDoor; }
        }

        public Effect ChangeColor
        {
            get { return changeColor; }
        }
    }
}