﻿using Signals.EventManager;
using UnityEngine;
using Utils;

public class DoorControlWithCubesPosition : DoorControlBase
{
    [SerializeField] private int _numberOfCubesInPositionToActivateDoor;
    private int _numberOfCubesInPosition;
    private IEventManager _eventManager;

    private void Awake()
    {
        _eventManager = ServiceLocator.Instance.GetService<IEventManager>();
    }

    void Start()
    {
        _portalEffect.gameObject.SetActive(false);

        _portalEffect.OnPlayerHitPortalCollider += OnPlayerEnter;
        
        IsActivated = false;
        _eventManager.AddActionToSignal<CubeIsInPlaceSignal>(CubeIsInPlace);
        _eventManager.AddActionToSignal<CubeIsNotInPlaceSignal>(CubeIsNotInPlace);
    }

    private void OnDestroy()
    {
        _portalEffect.OnPlayerHitPortalCollider -= OnPlayerEnter;
        _eventManager.RemoveActionFromSignal<CubeIsInPlaceSignal>(CubeIsInPlace);
        _eventManager.RemoveActionFromSignal<CubeIsNotInPlaceSignal>(CubeIsNotInPlace);
    }

    private void CubeIsNotInPlace(CubeIsNotInPlaceSignal obj)
    {
        if (IsActivated) return;
        _numberOfCubesInPosition--;
    }

    private void CubeIsInPlace(CubeIsInPlaceSignal obj)
    {
        if (IsActivated) return;
        _numberOfCubesInPosition++;
        if (_numberOfCubesInPosition < _numberOfCubesInPositionToActivateDoor) return;
        ActivateDoor();
    }
}