﻿using System;
using Presentation.Enemy;
using Presentation.Items;
using UnityEngine;

public class Cube : PickableItem, IExecuteAction
{
    [SerializeField] private BoxCollider _boxCollider;
    [SerializeField] private ActionToDo _actionToDoWhenIsInPlace, _actionToDoWhenIsHitWhenEnemy;
    [SerializeField] private float _timeToWaitToExecuteAction, _timeToDissolveCube;
    [SerializeField]  private Transform _originalPositionTransform;
    private float _distanceToObject;
    private bool _canMove, _playerIsInteractingWithCube;
    private Timer _timer;
    private Vector3 _originalPosition;

    private Renderer _renderer;

    private void Awake()
    {
        // Debug.Log($" AWAKE {gameObject.name}  {transform.position} {transform.localPosition}");
        _originalPosition = new Vector3(transform.position.x, transform.position.y, transform.position.z);
        _renderer = GetComponent<Renderer>();
    }

    private void Start()
    {
        _timer = new Timer();
        _timer.SetTimeToWait(_timeToWaitToExecuteAction);

        _playerIsInteractingWithCube = false;
        _distanceToObject = -1;
        _canMove = true;
    }

    private void OnCollisionEnter(Collision other)
    {
        if (!other.collider) return;
        var enemy = other.gameObject.CompareTag("Enemy");
        var wall = other.gameObject.CompareTag("Wall");
        if (enemy && _playerIsInteractingWithCube || wall)
        {
            StopInteraction();
            new PlayerMustWaitToGetCubeAgainSignal().Execute();

            DissolveByHit();
            return;
        }

        BoxChecker boxChecker = other.collider.GetComponent<BoxChecker>();
        if (boxChecker) return;
        _canMove = false;
    }

    private void MoveToOriginalPosition()
    {
        Debug.Log("MOVE");
    }


    public override void InteractWithObject(GameObject objectWhichInteract)
    {
        _playerIsInteractingWithCube = true;
        transform.parent = objectWhichInteract.transform;
    }


    public override void StopInteraction()
    {
        // Debug.Log("DEJAMOS CUBO");
        _distanceToObject = -1;
        _canMove = true;
        _playerIsInteractingWithCube = false;
        transform.parent = null;
    }

    public override bool PlayerIsInteractingWithIt()
    {
        return _playerIsInteractingWithCube;
    }

    public void ExecuteAction()
    {
        Debug.Log("EXECUTE");
        _timer.OnTimerEnds += DissolveWithAction;
        StartCoroutine(_timer.TimerCoroutine());
    }

    private void DissolveWithAction()
    {
        _timer.OnTimerEnds -= DissolveWithAction;
        _boxCollider.enabled = false;
        DissolveInfoCube dissolveInfoCube = new DissolveInfoCube()
        {
            Renderer = _renderer,
            TimeToDissolveCube = _timeToDissolveCube
        };

        _actionToDoWhenIsInPlace.Init(dissolveInfoCube);
        _actionToDoWhenIsInPlace.ExecuteAction();
        new PlayCubeVanishSoundSignal().Execute();
    }

    private void DissolveByHit()
    {
        _timer = new Timer();

        _boxCollider.enabled = false;
        DissolveInfoAndReturnPositionCube dissolveInfoCube = new DissolveInfoAndReturnPositionCube()
        {
            Renderer = _renderer,
            TimeToDissolveCube = _timeToDissolveCube,
            OriginalPosition = _originalPositionTransform.position,
            Transform = transform
        };

        _actionToDoWhenIsHitWhenEnemy.Init(dissolveInfoCube);
        _actionToDoWhenIsHitWhenEnemy.ExecuteAction();
        _timer.SetTimeToWait(_timeToDissolveCube);
        _timer.OnTimerEnds += ActivateCubeAgain;
        StartCoroutine(_timer.TimerCoroutine());

        new PlayCubeVanishSoundSignal().Execute();
    }

    private void ActivateCubeAgain()
    {
        transform.position = _originalPositionTransform.position;

        _timer.OnTimerEnds -= ActivateCubeAgain;
        gameObject.SetActive(true);
        _boxCollider.enabled = true;
    }
}