﻿// using UnityEngine;
//
// public class Door : MonoBehaviour
// {
//     [SerializeField] protected Animator _animator;
//
//     // [SerializeField] protected ColliderPassingChecker _colliderPassingChecker;
//     private static readonly int Close = Animator.StringToHash("Close");
//     private static readonly int Open = Animator.StringToHash("Open");
//     [SerializeField] protected bool _isOpen;
//     private bool _isMoving = false;
//
//
//     private void Start()
//     {
//         // _colliderPassingChecker.OnPlayerPassCollider += InteractWithObject;
//     }
//
//     private void OpenDoor()
//     {
//         _isMoving = true;
//         _animator.SetTrigger(Open);
//     }
//
//
//     // public override void InteractWithObject(GameObject objectWhichInteract)
//     // {
//     //     InteractWithObject();
//     // }
//
//     public void InteractWithObject()
//     {
//         if (_isMoving)
//         {
//             Debug.Log("FF");
//             return;
//         }
//
//         if (_isOpen)
//         {
//             CloseDoor();
//         }
//         else
//         {
//             OpenDoor();
//         }
//
//         _isOpen = !_isOpen;
//         Debug.Log("Interactuamos con Puerta");
//     }
//
//     private void CloseDoor()
//     {
//         _isMoving = true;
//
//         _animator.SetTrigger(Close);
//     }
//
//     protected virtual void DoorStopped()
//     {
//         _isMoving = false;
//     }
//
//     protected void OnDestroy()
//     {
//         // _colliderPassingChecker.OnPlayerPassCollider -= InteractWithObject;
//     }
// }