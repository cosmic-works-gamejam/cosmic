﻿using UnityEngine;

public class DoorControlWithTimer : DoorControlBase
{
    [SerializeField] private TimeControl _timerControl;

    private void Start()
    {
        _portalEffect.gameObject.SetActive(false);

        _timerControl.OnTimeHasPast += HandleTimeHasHasPast;
        _portalEffect.OnPlayerHitPortalCollider += OnPlayerEnter;
    }

    private void OnDestroy()
    {
        _timerControl.OnTimeHasPast -= HandleTimeHasHasPast;
        _portalEffect.OnPlayerHitPortalCollider -= OnPlayerEnter;
    }

    private void HandleTimeHasHasPast()
    {
        ActivateDoor();
    }
}