﻿using Presentation.Enemy;
using Signals;

public class ResetEnemyPathSignal : SignalBase
{
    public EnemyBaseFacade EnemyBaseFacade;
}