﻿using System;
using Signals.EventManager;
using UnityEngine;
using Utils;


public class AudioManagerInGameScene : MonoBehaviour
{
    [SerializeField] private AudioSource _soundTrack;
    [SerializeField] private AudioSource _cubeVanish;
    [SerializeField] private AudioSource _fadeLevel;
    [SerializeField] private AudioSource _gameOver;
    [SerializeField] private AudioSource _enemyOne;
    [SerializeField] private AudioSource _enemyTwo;
    [SerializeField] private AudioSource _boss;
    [SerializeField] private AudioSource _popUpHelp;
    [SerializeField] private AudioSource _doorActivate;
    [SerializeField] private AudioSource _restartLevel;
    [SerializeField] private AudioSource _jump;
    [SerializeField] private AudioSource _enemySawYou;
    [SerializeField] private AudioSource _clickButton;
    [SerializeField] private AudioSource _hitToBoss;
    [SerializeField] private AudioSource _bossDead;
    [SerializeField] private AudioSource _hitToEnemyTwo;


    private IEventManager _eventManager;

    void Awake()
    {
        _eventManager = ServiceLocator.Instance.GetService<IEventManager>();
    }

    private void Start()
    {
        _soundTrack.Play();
        _eventManager.AddActionToSignal<PlayCubeVanishSoundSignal>(PlayCubeVanish);
        _eventManager.AddActionToSignal<PlayGameOverSoundSignal>(PlayGameOver);
        _eventManager.AddActionToSignal<PlayEnemyOneNearSoundSignal>(PlayEnemyOneNear);
        _eventManager.AddActionToSignal<PlayEnemyTwoNearSoundSignal>(PlayEnemyTwoNear);
        _eventManager.AddActionToSignal<PlayEnemyBossNearSoundSignal>(PlayEnemyBossNear);
        _eventManager.AddActionToSignal<PlayPopupHelpSoundSignal>(PlayPopUpHelp);
        _eventManager.AddActionToSignal<PlayDoorActivateSoundSignal>(PlayDoorActivate);
        _eventManager.AddActionToSignal<PlayRestartLevelSoundSignal>(PlayRestartLevel);
        _eventManager.AddActionToSignal<PlayPlayerJumpSoundSignal>(PlayJump);
        _eventManager.AddActionToSignal<PlayEnemySeePlayerSoundSignal>(PlayEnemySawYou);
        _eventManager.AddActionToSignal<PlayFadeLevelSoundSignal>(PlayFadeLevel);
        _eventManager.AddActionToSignal<PlayClickButtonSignal>(PlayClickButton);
        _eventManager.AddActionToSignal<PlayHitBossSignal>(PlayHitBoss);
        _eventManager.AddActionToSignal<PlayBossDeadSignal>(PlayBossDead);
        _eventManager.AddActionToSignal<PlayHitToEnemyTwoSignal>(PlayHitToEnemyTwo);
    }

    private void PlayHitToEnemyTwo(PlayHitToEnemyTwoSignal obj)
    {
        if (_hitToEnemyTwo.isPlaying) return;
        _hitToEnemyTwo.Play();
    }

    private void PlayBossDead(PlayBossDeadSignal obj)
    {
        if (_bossDead.isPlaying) return;
        _bossDead.Play();
    }

    private void PlayHitBoss(PlayHitBossSignal obj)
    {
        if (_hitToBoss.isPlaying) return;
        _hitToBoss.Play();
    }

    private void PlayClickButton(PlayClickButtonSignal obj)
    {
        if (_clickButton.isPlaying) return;
        _clickButton.Play();
    }

    private void PlayFadeLevel(PlayFadeLevelSoundSignal obj)
    {
        if (_fadeLevel.isPlaying) return;
        _fadeLevel.Play();
    }

    private void PlayEnemyBossNear(PlayEnemyBossNearSoundSignal obj)
    {
        if (_boss.isPlaying) return;
        _boss.Play();
    }

    private void PlayPopUpHelp(PlayPopupHelpSoundSignal obj)
    {
        if (_popUpHelp.isPlaying) return;
        _popUpHelp.Play();
    }

    private void PlayDoorActivate(PlayDoorActivateSoundSignal obj)
    {
        if (_doorActivate.isPlaying) return;
        _doorActivate.Play();
    }

    private void PlayRestartLevel(PlayRestartLevelSoundSignal obj)
    {
        if (_restartLevel.isPlaying) return;
        _restartLevel.Play();
    }

    private void PlayEnemySawYou(PlayEnemySeePlayerSoundSignal obj)
    {
        if (_enemySawYou.isPlaying) return;
        _enemySawYou.Play();
    }

    private void PlayJump(PlayPlayerJumpSoundSignal obj)
    {
        if (_jump.isPlaying) return;
        _jump.Play();
    }

    private void PlayEnemyTwoNear(PlayEnemyTwoNearSoundSignal obj)
    {
        if (_enemyTwo.isPlaying) return;
        _enemyTwo.Play();
    }

    private void PlayEnemyOneNear(PlayEnemyOneNearSoundSignal obj)
    {
        if (_enemyOne.isPlaying) return;
        _enemyOne.Play();
    }

    private void PlayGameOver(PlayGameOverSoundSignal obj)
    {
        if (_gameOver.isPlaying) return;
        _gameOver.Play();
    }

    private void PlayCubeVanish(PlayCubeVanishSoundSignal obj)
    {
        if (_cubeVanish.isPlaying) return;
        _cubeVanish.Play();
    }
}