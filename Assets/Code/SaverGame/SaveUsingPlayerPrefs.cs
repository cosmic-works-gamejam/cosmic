﻿using Domain.JsonTranslator;
using UnityEngine;
using Utils;

namespace Domain.SaverGame
{
    public class SaveUsingPlayerPrefs : ISaver
    {
        private IJsonator _jsonator;

        public SaveUsingPlayerPrefs()
        {
            _jsonator = ServiceLocator.Instance.GetService<IJsonator>();
        }

        public void SaveGame(SaveGameData saveGameData)
        {
            string dataToSaveJson = _jsonator.ToJson(saveGameData);
            PlayerPrefs.SetString("SaveGame", dataToSaveJson);
            PlayerPrefs.SetInt("HasSavedGame", 1);
            PlayerPrefs.Save();
        }

        public void DeleteSaveGame()
        {
            PlayerPrefs.DeleteKey("SaveGame");
            PlayerPrefs.DeleteKey("HasSavedGame");
            PlayerPrefs.Save();
        }

        public void SaveNewGameStatus(bool statusToSave)
        {
            PlayerPrefs.SetInt("HasSavedGame", statusToSave ? 1 : 0);
        }
        
    }
}