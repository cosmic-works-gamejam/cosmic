﻿namespace Domain.SaverGame
{
    public interface ISaver
    {
        void SaveGame(SaveGameData saveGameData);
        void DeleteSaveGame();
        void SaveNewGameStatus(bool statusToSave);
    }
}