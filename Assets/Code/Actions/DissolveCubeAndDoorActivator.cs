﻿using DG.Tweening;
using UnityEngine;
using VFX;

[CreateAssetMenu(fileName = "DissolveCubeAndDoorActivator", menuName = "ActionsToDo/DissolveCubeAndDoorActivator")]
public class DissolveCubeAndDoorActivator : ActionToDo
{
    private Renderer _renderer;
    private float _timeToDissolveObject;

    public override void Init(ActionInfo actionInfo)
    {
        if(actionInfo == null) return;
        DissolveInfoCube dissolveInfoCube = (DissolveInfoCube) actionInfo;
        _renderer = dissolveInfoCube.Renderer;
        _timeToDissolveObject = dissolveInfoCube.TimeToDissolveCube;
    }

    public override void ExecuteAction()
    {
        new CubeIsInPlaceSignal().Execute();

        Sequence sequence = ObjectEffect.Singleton.OneTimeEffect(_renderer, ObjectEffect.Singleton.Dissolve, _timeToDissolveObject,0, 1);
        sequence.onComplete += DestroyObject;
    }

    private void DestroyObject()
    {
        Destroy(_renderer.gameObject);
    }
}