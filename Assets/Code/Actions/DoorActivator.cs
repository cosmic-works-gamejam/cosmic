﻿using UnityEngine;

[CreateAssetMenu(fileName = "OpenDoor", menuName = "ActionsToDo/DoorActivator")]
public class DoorActivator : ActionToDo
{
    private bool _hasBeenExecutedBefore;

    public override void Init(ActionInfo actionInfo)
    {
        if (actionInfo == null) return;
    }

    public override void ExecuteAction()
    {
        if (_hasBeenExecutedBefore)
        {
            new CubeIsNotInPlaceSignal().Execute();
            return;
        }

        new CubeIsInPlaceSignal().Execute();
    }
}
