﻿using UnityEngine;

[CreateAssetMenu(fileName = "OpenDoor1", menuName = "ActionsToDo/OpenDoor1")]
public class OpenDoorByCube : ActionToDo
{
    private bool _hasBeenExecutedBefore;

    public override void Init(ActionInfo actionInfo)
    {
        if (actionInfo == null) return;
    }

    public override void ExecuteAction()
    {
        if (_hasBeenExecutedBefore)
        {
            new CubeIsNotInPlaceSignal().Execute();
            return;
        }

        new CubeIsInPlaceSignal().Execute();
    }
}