﻿using DG.Tweening;
using UnityEngine;
using VFX;

[CreateAssetMenu(fileName = "DissolveCubeAndGoToOriginalPosition",
    menuName = "ActionsToDo/DissolveCubeAndGoToOriginalPosition")]
public class DissolveCubeAndGoToOriginalPosition : ActionToDo
{
    private Renderer _renderer;
    private float _timeToDissolveObject;
    private Transform _transform;
    private Vector3 _originalPosition;

    public override void Init(ActionInfo actionInfo)
    {
        if (actionInfo == null) return;
        DissolveInfoAndReturnPositionCube dissolveInfoCube = (DissolveInfoAndReturnPositionCube) actionInfo;
        _renderer = dissolveInfoCube.Renderer;
        _timeToDissolveObject = dissolveInfoCube.TimeToDissolveCube;
        _originalPosition = dissolveInfoCube.OriginalPosition;
        _transform = dissolveInfoCube.Transform;
    }

    public override void ExecuteAction()
    {
        Sequence sequence =
            ObjectEffect.Singleton.OneTimeEffect(_renderer, ObjectEffect.Singleton.Dissolve, _timeToDissolveObject,0, 1);
        sequence.onComplete += MoveToOriginalPosition;
    }

    private void MoveToOriginalPosition()
    {
        Debug.Log($" MOVE to {_originalPosition}");

        // _transform.localPosition = _originalPosition.;
        Sequence sequence =
            ObjectEffect.Singleton.OneTimeEffect(_renderer, ObjectEffect.Singleton.Dissolve, _timeToDissolveObject,1,0);
    }
}