﻿using DG.Tweening;
using UnityEngine;
using VFX;

[CreateAssetMenu(fileName = "DissolveCube", menuName = "ActionsToDo/DissolveCube")]
public class DissolveCubeAndDestroy : ActionToDo
{
    private Renderer _renderer;
    private float _timeToDissolveObject;

    public override void Init(ActionInfo actionInfo)
    {
        if(actionInfo == null) return;
        DissolveInfoCube dissolveInfoCube = (DissolveInfoCube) actionInfo;
        _renderer = dissolveInfoCube.Renderer;
        _timeToDissolveObject = dissolveInfoCube.TimeToDissolveCube;
    }

    public override void ExecuteAction()
    {
        Sequence sequence = ObjectEffect.Singleton.OneTimeEffect(_renderer, ObjectEffect.Singleton.Dissolve, _timeToDissolveObject, 1,0);
        sequence.onComplete += DestroyObject;
    }

    private void DestroyObject()
    {
        Destroy(_renderer.gameObject);
    }
}