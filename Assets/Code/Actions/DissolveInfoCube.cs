﻿using UnityEngine;

public class DissolveInfoCube : ActionInfo
{
    public Renderer Renderer;
    public float TimeToDissolveCube;
}

public class DissolveInfoAndReturnPositionCube : ActionInfo
{
    public Renderer Renderer;
    public float TimeToDissolveCube;
    public Vector3 OriginalPosition;
    public Transform Transform;
}