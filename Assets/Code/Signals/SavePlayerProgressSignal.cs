﻿using Signals;

public class SavePlayerProgressSignal : SignalBase
{
    public int IdSceneToSave;
    public CharacterName CharacterName;
}