﻿using System;
using System.Collections.Generic;

[Serializable]
public struct SceneInfoByCharacter
{
    public List<SceneInfo> SceneInfo;
    public CharacterName CharacterName;
}