﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ScenesListModel : MonoBehaviour
{
    [SerializeField] private List<SceneInfoByCharacter> SceneInfos;

    public SceneInfo SceneInfoByIdAndCharacter(int id, CharacterName characterName)
    {
        var sceneInfoByCharacter = GetScenesByCharacterName(characterName);
        return GetSceneById(sceneInfoByCharacter, id);
    
    }

    public SceneInfo SceneInfoByName(string sceneName, CharacterName characterName)
    {
        var sceneInfoByCharacter = GetScenesByCharacterName(characterName);
        if (sceneInfoByCharacter.SceneInfo == null || sceneInfoByCharacter.SceneInfo.Count < 1)
        {
            throw new Exception($"Scene Info Of {sceneInfoByCharacter} is empty");
        }

        return GetSceneByName(sceneInfoByCharacter, sceneName);
    }

    private SceneInfo GetSceneById(SceneInfoByCharacter sceneInfoByCharacter, int id)
    {
        var sceneInfo = sceneInfoByCharacter.SceneInfo.SingleOrDefault(x => x.SceneId == id);
        if (string.IsNullOrEmpty(sceneInfo.SceneName))
        {
            throw new Exception($"Scene Info Of {sceneInfo.SceneName} is empty");
        }

        return sceneInfo;
    }

    private SceneInfo GetSceneByName(SceneInfoByCharacter sceneInfoByCharacter, string sceneName)
    {
        var sceneInfo = sceneInfoByCharacter.SceneInfo.Single(x => x.SceneName == sceneName);
        if (sceneInfo.SceneId < 0)
        {
            throw new Exception($"Scene Info Of {sceneName} is < 0");
        }

        return sceneInfo;
    }

    public SceneInfo GetNextScene(string currentSceneName, CharacterName characterName)
    {
        int idCurrentScene = SceneInfoByName(currentSceneName, characterName).SceneId;
        var sceneInfoByCharacter = GetScenesByCharacterName(characterName);
        if (idCurrentScene < 0)
        {
            throw new Exception($"Id of Scene is < 0");

        }

        if (idCurrentScene + 1 < sceneInfoByCharacter.SceneInfo.Count)
        {
            idCurrentScene++;
        }
        else
        {
            return new SceneInfo {SceneName = "Credits"};
        }

        return GetSceneById(sceneInfoByCharacter, idCurrentScene);
    }

    public int GetSceneCount(CharacterName characterName)
    {
        return SceneInfos.Single(x => x.CharacterName == characterName).SceneInfo.Count;
    }

    public SceneInfoByCharacter GetScenesByCharacterName(CharacterName characterName)
    {
        var sceneInfo = SceneInfos.Single(x => x.CharacterName == characterName);
        if (sceneInfo.SceneInfo.Count < 0)
        {
            throw new Exception($"Scene List Of {characterName} is empty");
        }

        return sceneInfo;
    }
}