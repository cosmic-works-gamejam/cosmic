﻿using System;
using App.PlayerModelInfo;
using DG.Tweening;
using Signals.EventManager;
using UnityEngine;
using UnityEngine.SceneManagement;
using Utils;
using VFX;

public class SceneChanger : MonoBehaviour
{
    [SerializeField] private ScenesListModel _scenesListModel;
    private IEventManager _eventManager;
    private Sequence _sequence;
    private IPlayerModel _playerModel;
    private ISceneModel _sceneModel;

    void Awake()
    {
        _eventManager = ServiceLocator.Instance.GetService<IEventManager>();
        _playerModel = ServiceLocator.Instance.GetModel<IPlayerModel>();
        _sceneModel = ServiceLocator.Instance.GetModel<ISceneModel>();
    }

    private void Start()
    {
        _eventManager.AddActionToSignal<PlayerHasCompletedLevelSignal>(HandleCompletedLevel);
    }

    private void OnDestroy()
    {
        _eventManager.RemoveActionFromSignal<PlayerHasCompletedLevelSignal>(HandleCompletedLevel);
    }

    private void HandleCompletedLevel(PlayerHasCompletedLevelSignal obj)
    {
        _sequence = LevelEffect.Singleton.DissolveLevelEffect();
        _sequence.onComplete += ChangeLevel;
    }
    
    private void ChangeLevel()
    {
        _sequence.onComplete -= ChangeLevel;
        SceneInfo nextSceneInfo = _scenesListModel.GetNextScene(GetCurrentSceneName(), _playerModel.CharacterNameSelected);
        SceneInfo currentSceneInfo =
            _scenesListModel.SceneInfoByName(GetCurrentSceneName(), _playerModel.CharacterNameSelected);
        new SavePlayerProgressSignal()
        {
            IdSceneToSave = currentSceneInfo.SceneId,
            CharacterName = _playerModel.CharacterNameSelected
        }.Execute();
        Debug.Log("Has pasado de nivel");
        new PlayFadeLevelSoundSignal().Execute();
        
        _sceneModel.PreviousScene = GetCurrentSceneName();
        _sceneModel.NextScene = nextSceneInfo.SceneName;
        
        SceneManager.LoadScene(nextSceneInfo.SceneName);
        // var operation = SceneManager.LoadSceneAsync(_sceneModel.LoadingScene, LoadSceneMode.Additive);
        // operation.completed += ChangeScene;
    }

    private void ChangeScene(AsyncOperation obj)
    {
        throw new NotImplementedException();
    }

    public void RestartScene()
    {
        SceneManager.LoadScene(GetCurrentSceneName());
    }

    public void GoToFirstLevel(CharacterName characterName)
    {
        SceneManager.LoadScene(_scenesListModel.GetScenesByCharacterName(characterName).SceneInfo[0].SceneName);
    }

    public void GoToInitScene()
    {
        SceneManager.LoadScene("Init");
    }

    public String GetCurrentSceneName()
    {
        return SceneManager.GetActiveScene().name;
    }

    public void LoadCurrentLevelFromInitScene(int gameDataLastLevelCompleted, CharacterName characterName)
    {
        SceneManager.LoadScene(_scenesListModel.SceneInfoByIdAndCharacter(gameDataLastLevelCompleted, characterName)
            .SceneName);
    }
    
}