﻿public class DoorOpened : DoorControlBase
{
    void Start()
    {
        _portalEffect.gameObject.SetActive(false);
        ActivateDoor();
        _portalEffect.OnPlayerHitPortalCollider += OnPlayerEnter;
    }

    private void OnDestroy()
    {
        if(_portalEffect)
            _portalEffect.OnPlayerHitPortalCollider -= OnPlayerEnter;
    }
}