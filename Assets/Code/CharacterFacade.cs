﻿using UnityEngine;

namespace Presentation.Player
{
    public abstract class CharacterFacade : MonoBehaviour
    {
        [SerializeField] private float _movementSpeed;
        private Vector3 _vectorOfMovement;

        public abstract Vector3 VectorOfMovement();
        public abstract Animator GetAnimator();
        

        protected float MovementSpeed
        {
            get => _movementSpeed;
            set => _movementSpeed = value;
        }
    }
}