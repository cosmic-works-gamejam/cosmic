﻿using System;
using FSM;
using Presentation.Enemy.IA;
using Presentation.Player;
using Signals.EventManager;
using UnityEngine;
using Utils;


namespace Presentation.Enemy
{
    public abstract class EnemyBaseFacade : CharacterFacade
    {
        [SerializeField] protected EnemyMovement _enemyMovement;
        [SerializeField] private ObjectDetector _objectDetector;
        [SerializeField] private Animator _animator;
        [SerializeField] private PatrolMovement _patrolMovement;
        [SerializeField] protected internal bool IsAlive;
        protected IEventManager _eventManager;
        protected State _state;
        public EnemyMovement EnemyMovement => _enemyMovement;
        public float DistanceToPlayEnemyNearPlayerSound;

        protected virtual void Awake()
        {
            _eventManager = ServiceLocator.Instance.GetService<IEventManager>();
        }



        public virtual void Start()
        {
            _eventManager.AddActionToSignal<DeactivateEnemiesSignal>(StopEnemy);
            EnemyMovement.SetMovementSpeed(MovementSpeed);
            IsAlive = true;
        }

        private void OnDestroy()
        {
            _eventManager.RemoveActionFromSignal<DeactivateEnemiesSignal>(StopEnemy);
        }

        private void StopEnemy(DeactivateEnemiesSignal obj)
        {
            IsAlive = false;
        }
        
        public override Animator GetAnimator()
        {
            return _animator;
        }

        public ObjectDetector GetObjectDetector()
        {
            return _objectDetector;
        }

        public IPatrol GetPatrol()
        {
            return _patrolMovement;
        }


        public override Vector3 VectorOfMovement()
        {
            return _enemyMovement.VectorOfMovement;
        }

        public abstract void PlaySoundEnemyIsNearEnemy();

        public void ResetPath()
        {
            _enemyMovement.ResetPath();
        }

        public void CalculatePath()
        {
            _enemyMovement.CalculatePath();
        }

        public void CalculatePathUpdatingPatrolPoint()
        {
            _enemyMovement.CalculatePathUpdatingPatrolPoint();
        }
    }
}