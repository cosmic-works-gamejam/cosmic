﻿using UnityEngine;

namespace Presentation.Enemy.IA
{
    public interface IPatrol
    {
        // bool IsNearObjectivePatrolPoint();
        // public void UpdatePatrolPoint();
        public void SetNewPatrolPoint();
        
        Transform GetCurrentPatrolPoint();
        // void ChangePatrolPointSet(int changePatrolSetToChange);
        // bool PatrolPointIsObjectivePatrolPoint();
    }
}