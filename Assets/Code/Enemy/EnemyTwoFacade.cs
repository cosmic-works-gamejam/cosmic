﻿using FSM;
using Presentation.Enemy;
using Signals;
using Signals.EventManager;
using UnityEngine;
using UnityEngine.Assertions;
using Utils;
using VFX;

public class EnemyTwoFacade : EnemyBaseFacade, IPunchable
{
    
    private void Start()
    {
        base.Start();

        _state = new Idle(this);
    }

    public override void PlaySoundEnemyIsNearEnemy()
    {
        new PlayEnemyTwoNearSoundSignal().Execute();
    }

    private void Update()
    {
        if (IsAlive)
        {
            _state = _state.Process();
            _enemyMovement.SetMovementSpeed(MovementSpeed);
            Debug.Log($"{gameObject.name} {_state}");
        }
        else
        {
            _enemyMovement.SetMovementSpeed(0);
        }
    }

    void Awake()
    {
        base.Awake();
        Assert.IsNotNull(_enemyMovement);
        _eventManager = ServiceLocator.Instance.GetService<IEventManager>();
        _enemyMovement.SetPatrolLogic(GetPatrol());
        _enemyMovement.SetMovementSpeed(MovementSpeed);
    }

    public void Punch(Transform objectWhichHit)
    {
        var forward = objectWhichHit.forward;
        var areOpposite = Vector3Utils.VectorsAreInOppositeDirection(transform.forward, forward);
        if (areOpposite)
        {
            new PlayerHasBeenCaughtByEnemySignal().Execute();

            return;
        }

        ObjectEffect.Singleton.OneTimeEffect(gameObject.transform, ObjectEffect.Singleton.EnemyDead, 2);
        new PlayHitToEnemyTwoSignal().Execute();
        IsAlive = false;
        Destroy(gameObject);
    }
}