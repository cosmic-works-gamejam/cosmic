﻿using UnityEngine;

namespace Utils
{
    public static class LayerMaskUtils
    {
        public static bool LayerIsInLayersSet(LayerMask layerMaskSetToCheck, int layerToCheck)
        {
            return (layerMaskSetToCheck & 1 << layerToCheck) != 0;
        }
    }
}