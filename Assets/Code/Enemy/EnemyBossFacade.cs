﻿using DG.Tweening;
using FSM;
using Presentation.Enemy;
using UnityEngine;
using VFX;

public class EnemyBossFacade : EnemyBaseFacade, IPunchable
{
    [SerializeField] private int _numOfLives;
    [SerializeField] private float _velocityAfterTwoHits;

    void Awake()
    {
        _enemyMovement.SetPatrolLogic(GetPatrol());
        _enemyMovement.SetPatrolLogic(GetPatrol());
        _enemyMovement.SetMovementSpeed(MovementSpeed);
    }

    private void Start()
    {
        _state = new Idle(this);
        // _eventManager.AddActionToSignal<PlayerHasTouchedBoxSignal>(PlayerHasTouchedBox);
    }

    public override void PlaySoundEnemyIsNearEnemy()
    {
        new PlayEnemyBossNearSoundSignal().Execute();
    }

    private void Update()
    {
        if (IsAlive)
        {
            _state = _state.Process();
            _enemyMovement.SetMovementSpeed(MovementSpeed);
        }
        else
        {
            _enemyMovement.SetMovementSpeed(0);
        }
    }


    public void Punch(Transform objectWhichHit)
    {
        _numOfLives--;
        new PlayHitBossSignal().Execute();
        if (_numOfLives == _numOfLives - 2)
        {
            _enemyMovement.SetMovementSpeed(_velocityAfterTwoHits);
        }

        if (_numOfLives > 0) return;
        ObjectEffect.Singleton.OneTimeEffect(gameObject.transform, ObjectEffect.Singleton.EnemyDead, 2);

        new PlayBossDeadSignal().Execute();
        new ActivateDoorSignal().Execute();
        var sequence = ObjectEffect.Singleton.OneTimeEffect(gameObject.GetComponent<MeshRenderer>(),
            ObjectEffect.Singleton.Dissolve, 1,0, 1);
        sequence.onComplete += () => Destroy(gameObject);
    }
}