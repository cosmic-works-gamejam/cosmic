﻿using FSM;
using Signals;
using Signals.EventManager;
using UnityEngine;
using UnityEngine.Assertions;
using Utils;

namespace Presentation.Enemy
{
    public class EnemyOneFacade : EnemyBaseFacade, IPunchable
    {
        private void Start()
        {
            base.Start();
            _state = new Idle(this);
            // _eventManager.AddActionToSignal<PlayerHasTouchedBoxSignal>(PlayerHasTouchedBox);
        }

        public override void PlaySoundEnemyIsNearEnemy()
        {
            new PlayEnemyOneNearSoundSignal().Execute();
        }


        private void Update()
        {
            if (IsAlive)
            {
                _state = _state.Process();
                _enemyMovement.SetMovementSpeed(MovementSpeed);
                Debug.Log($"{gameObject.name} {_state}");

            }
            else
            {
                _enemyMovement.SetMovementSpeed(0);
            }
        }

        void Awake()
        {
            base.Awake();
            Assert.IsNotNull(_enemyMovement);
            _eventManager = ServiceLocator.Instance.GetService<IEventManager>();
            _enemyMovement.SetPatrolLogic(GetPatrol());
            _enemyMovement.SetMovementSpeed(MovementSpeed);
        }

        public void Punch(Transform objectWhichHit)
        {
            new PlayerHasBeenCaughtByEnemySignal().Execute();
        }
    }
}