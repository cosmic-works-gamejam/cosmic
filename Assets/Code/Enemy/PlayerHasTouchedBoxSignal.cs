﻿using Signals;

public class PlayerHasTouchedBoxSignal : SignalBase
{
    public int IdPatrolPointToChange;
}