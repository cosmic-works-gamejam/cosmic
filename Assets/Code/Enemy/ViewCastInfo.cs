﻿using UnityEngine;

public struct ViewCastInfo
{
    public bool hit;
    public Vector3 point;
    public float distanceOfRay;
    public float angle;
    public LayerMask layerMask;
    public GameObject colliderGameObject;

    public ViewCastInfo(bool hit, Vector3 point, float distanceOfRay, float angle, LayerMask layerMask,
        GameObject colliderGameObject)
    {
        this.hit = hit;
        this.point = point;
        this.distanceOfRay = distanceOfRay;
        this.angle = angle;
        this.layerMask = layerMask;
        this.colliderGameObject = colliderGameObject;
    }
}