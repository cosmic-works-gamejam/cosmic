﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Utils;

namespace Presentation.Enemy.IA
{
    [Serializable]
    public struct PatrolPoints
    {
        public List<Transform> PatrolPointsSet;
    }

    public class PatrolMovement : MonoBehaviour, IPatrol
    {
        // [SerializeField] private float _distanceToChangePatrolPoint;
        [SerializeField] private List<PatrolPoints> _patrolPositions;
        [SerializeField] private Transform _currentPatrolPoint;
        private int _currentPatrolPointId;
        private List<Transform> _currentPatrolPositionsSet;

        private void Awake()
        {
            Transform GetFirstPatrolPointOfFirstSet()
            {
                return _patrolPositions[0].PatrolPointsSet[0];
            }

            _currentPatrolPoint = GetFirstPatrolPointOfFirstSet();
            _currentPatrolPointId = 0;
            _currentPatrolPositionsSet = _patrolPositions[0].PatrolPointsSet;
        }

        //TODO TEST
        public void ChangePatrolPointSet(int patrolPointSetIdToChange)
        {
            _currentPatrolPositionsSet = _patrolPositions[patrolPointSetIdToChange].PatrolPointsSet;
            var nearestPatrolPointToEnemy = SearchNearestPatrolPointToEnemy();
            _currentPatrolPoint = nearestPatrolPointToEnemy;
        }

        // public bool PatrolPointIsObjectivePatrolPoint()
        // {
        //     //TODO COMPROBAR CON EL SIGUIENTE MAS QUE CON ESTA COMPROBACION, PUEDE DAR BUGS
        //     return SearchNearestPatrolPointToEnemy() == _currentPatrolPositionsSet[_currentPatrolPointId];
        // }

        private Transform SearchNearestPatrolPointToEnemy()
        {
            float distanceMin = Mathf.Infinity;
            int patrolPointId = -1;
            for (var i = 0; i < _currentPatrolPositionsSet.Count; i++)
            {
                float distanceToPatrolPoint =
                    Vector3.Distance(_currentPatrolPositionsSet[i].position, transform.position);
                if (distanceToPatrolPoint < distanceMin)
                {
                    distanceMin = distanceToPatrolPoint;
                    patrolPointId = i;
                }
            }

            return _currentPatrolPositionsSet[patrolPointId];
        }

        // private int GetIdPatrolPoint(Transform currentPatrolPoint)
        // {
        //     for (var i = 0; i < _currentPatrolPositionsSet.Count - 1; i++)
        //     {
        //         if (currentPatrolPoint == _currentPatrolPositionsSet[i])
        //         {
        //             return i;
        //         }
        //     }
        //
        //     return -1;
        // }

        public Transform GetCurrentPatrolPoint()
        {
            return _currentPatrolPoint;
        }

        // public bool IsNearObjectivePatrolPoint()
        // {
        //     return Vector3Utils.Vector3Distance(transform.position, _currentPatrolPoint.position,
        //         _distanceToChangePatrolPoint);
        // }

        // public void UpdatePatrolPoint()
        // {
        //     new ResetEnemyPathSignal()
        //     {
        //         EnemyBaseFacade = transform.GetComponent<EnemyBaseFacade>(),
        //     }.Execute();
        //     
        // }

        public void SetNewPatrolPoint()
        {
            _currentPatrolPointId++;
            
            if (IdPatrolPointIsGreaterThanPatrolPointsList(_currentPatrolPointId))
            {
                _currentPatrolPointId = 0;
            }
            
            SetPatrolPointById(_currentPatrolPointId);
        }

        private bool IdPatrolPointIsGreaterThanPatrolPointsList(int idPatrolPoint)
        {
            return idPatrolPoint >= _currentPatrolPositionsSet.Count;
        }

        private void SetPatrolPointById(int idPatrolPoint)
        {
            _currentPatrolPoint = _currentPatrolPositionsSet[idPatrolPoint];
        }
    }
}