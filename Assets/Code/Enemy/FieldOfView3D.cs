﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using JetBrains.Annotations;
using Utils;

public class FieldOfView3D : MonoBehaviour
{
    public float viewRadius;
    [Range(0, 360)] public float viewAngle;

    public LayerMask targetMask;
    public LayerMask obstacleMask;
    [HideInInspector] public List<Transform> visibleTargets = new List<Transform>();

    public float raysToDrawInMesh, edgeDistanceThreshold;
    public int edgeResolveIterations;

    public MeshFilter viewMeshFilter;
    private Mesh viewMesh;

    public event Action OnResetHitItem = delegate { };
    public event Action<Transform> OnHitSomething = delegate { };

    void Start()
    {
        viewMesh = new Mesh {name = "View Mesh"};
        viewMeshFilter.mesh = viewMesh;

        StartCoroutine(nameof(FindTargetsWithDelay), .2f);
    }


    IEnumerator FindTargetsWithDelay(float delay)
    {
        while (true)
        {
            yield return new WaitForSeconds(delay);
            FindVisibleTargets();
        }
    }

    void LateUpdate()
    {
        DrawFieldOfView();
    }

    private void FindVisibleTargets()
    {
        visibleTargets.Clear();
        Collider[] targetsInViewRadius = Physics.OverlapSphere(transform.position, viewRadius, targetMask);

        for (int i = 0; i < targetsInViewRadius.Length; i++)
        {
            Transform target = targetsInViewRadius[i].transform;
            Vector3 directionToTarget = (target.position - transform.position).normalized;
            if (Vector3.Angle(transform.forward, directionToTarget) < viewAngle / 2)
            {
                float distanceToTarget = Vector3.Distance(transform.position, target.position);
                if (!Physics.Raycast(transform.position, directionToTarget, distanceToTarget, obstacleMask))
                {
                    visibleTargets.Add(target);
                }
            }
        }

        if (visibleTargets.Count != 0) return;
        OnResetHitItem.Invoke();
    }

    void DrawFieldOfView()
    {
        int stepCount = Mathf.RoundToInt(viewAngle * raysToDrawInMesh);
        float stepAngleSize = viewAngle / stepCount;
        List<Vector3> viewPoints = new List<Vector3>();
        ViewCastInfo oldViewCast = new ViewCastInfo();
        for (int step = 0; step <= stepCount; step++)
        {
            float angle = transform.eulerAngles.y - viewAngle / 2 + stepAngleSize * step;
            // Debug.DrawLine(transform.position, transform.position + DirectionFromAngle(angle, true) * viewRadius,
            //     Color.yellow);

            var newViewCast = ViewCast(angle);

            if (step > 0)
            {
                bool edgeDstThresholdExceeded = Mathf.Abs(oldViewCast.distanceOfRay - newViewCast.distanceOfRay) >
                                                edgeDistanceThreshold;
                if (oldViewCast.hit != newViewCast.hit ||
                    (oldViewCast.hit && newViewCast.hit && edgeDstThresholdExceeded))
                {
                    EdgeInfo edge = FindEdge(oldViewCast, newViewCast);
                    if (edge.pointA != Vector3.zero)
                    {
                        viewPoints.Add(edge.pointA);
                    }

                    if (edge.pointB != Vector3.zero)
                    {
                        viewPoints.Add(edge.pointB);
                    }
                }
            }

            if (newViewCast.layerMask == targetMask)
            {
                OnHitSomething.Invoke(newViewCast.colliderGameObject.transform);
            }

            viewPoints.Add(newViewCast.point);
            oldViewCast = newViewCast;
        }

        // Debug.Log(viewPointsWithHitOfTargetLayer);

        int vertexCount = viewPoints.Count + 1;
        Vector3[] vertices = new Vector3[vertexCount];
        int[] triangles = new int[(vertexCount - 2) * 3];

        vertices[0] = Vector3.zero;
        for (int i = 0; i < vertexCount - 1; i++)
        {
            vertices[i + 1] = transform.InverseTransformPoint(viewPoints[i]);

            if (i < vertexCount - 2)
            {
                triangles[i * 3] = 0;
                triangles[i * 3 + 1] = i + 1;
                triangles[i * 3 + 2] = i + 2;
            }
        }

        viewMesh.Clear();

        viewMesh.vertices = vertices;
        viewMesh.triangles = triangles;
        viewMesh.RecalculateNormals();
    }


    EdgeInfo FindEdge(ViewCastInfo minViewCast, ViewCastInfo maxViewCast)
    {
        float minAngle = minViewCast.angle;
        float maxAngle = maxViewCast.angle;
        Vector3 minPoint = Vector3.zero;
        Vector3 maxPoint = Vector3.zero;

        for (int iteration = 0; iteration < edgeResolveIterations; iteration++)
        {
            float angle = (minAngle + maxAngle) / 2;
            ViewCastInfo newViewCast = ViewCast(angle);

            bool edgeDstThresholdExceeded =
                Mathf.Abs(minViewCast.distanceOfRay - newViewCast.distanceOfRay) > edgeDistanceThreshold;
            if (newViewCast.hit == minViewCast.hit && !edgeDstThresholdExceeded)
            {
                minAngle = angle;
                minPoint = newViewCast.point;
            }
            else
            {
                maxAngle = angle;
                maxPoint = newViewCast.point;
            }
        }

        return new EdgeInfo(minPoint, maxPoint);
    }

    bool CouldHitSomethingOnTargetLayer(Vector3 positionHit, out RaycastHit raycastHit)
    {
        var positionSeeker = transform.position;
        return Physics.Raycast(positionSeeker, positionHit, out raycastHit, viewRadius, targetMask);
    }

    bool HitSomethingOnObstacleLayer(Vector3 vector3, out RaycastHit raycastHit)
    {
        return Physics.Raycast(transform.position, vector3, out raycastHit, viewRadius, obstacleMask);
    }

    ViewCastInfo ViewCast(float globalAngle)
    {
        Vector3 direction = DirectionFromAngle(globalAngle, true);

        if (CouldHitSomethingOnTargetLayer(direction, out var hit))
        {
            var raycastHit = Physics.RaycastAll(transform.position, direction, hit.distance);
            if (FirstHitIsOfTargetLayerOrLayers(raycastHit))
            {
                return new ViewCastInfo(true, hit.point, hit.distance, globalAngle, targetMask,
                    hit.collider.gameObject);
            }
        }

        if (HitSomethingOnObstacleLayer(direction, out hit))
        {
            return new ViewCastInfo(true, hit.point, hit.distance, globalAngle, obstacleMask, hit.collider.gameObject);
        }

        return new ViewCastInfo(false, transform.position + direction * viewRadius, viewRadius, globalAngle, -1, null);
    }

    private bool FirstHitIsOfTargetLayerOrLayers([NotNull] RaycastHit[] raycastHit)
    {
        if (raycastHit == null) throw new ArgumentNullException(nameof(raycastHit));
        return LayerMaskUtils.LayerIsInLayersSet(targetMask, raycastHit[0].collider.gameObject.layer);
    }

    public Vector3 DirectionFromAngle(float angleInDegrees, bool angleIsGlobal)
    {
        if (!angleIsGlobal)
        {
            angleInDegrees += transform.eulerAngles.y;
        }

        return new Vector3(Mathf.Sin(angleInDegrees * Mathf.Deg2Rad), 0, Mathf.Cos(angleInDegrees * Mathf.Deg2Rad));
    }
}