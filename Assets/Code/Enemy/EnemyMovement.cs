﻿using System;
using Presentation.Enemy.IA;
using UnityEngine;
using UnityEngine.AI;

namespace Presentation.Enemy
{
    public class EnemyMovement : MonoBehaviour
    {
        [SerializeField] private NavMeshAgent _navMeshAgent;
        private IPatrol _patrolMovement;
        private bool _canMove;
        private Vector3 _direction;
        private float _movementSpeed;
        public event Action<Vector3> OnEnemyChangesDirection = delegate { };

        void Start()
        {
            _canMove = true;
        }


        public Vector3 VectorOfMovement => _direction;

        public void SetMovementSpeed(float movementSpeed)
        {
            _movementSpeed = movementSpeed;
            _navMeshAgent.speed = _movementSpeed;
        }

        private void SetPositionToGo(Transform positionToGo)
        {
            _direction = positionToGo.position - transform.position;

            OnEnemyChangesDirection.Invoke(_direction);
        }

        public void StartPatrolling()
        {
            var initialPatrolPoint = _patrolMovement.GetCurrentPatrolPoint();
            SetPositionToGo(initialPatrolPoint);
        }

        public void SetPatrolLogic(IPatrol patrolMovement)
        {
            _patrolMovement = patrolMovement;
        }

        public void GoToPosition(Transform getCurrentPatrolPoint)
        {
            NavMeshPath path = new NavMeshPath();
            _navMeshAgent.CalculatePath(getCurrentPatrolPoint.position, path);
            Debug.Log($"Path Status {path.status} Enemy {gameObject.name}");
            // bool setDestination =
                _navMeshAgent.destination  = getCurrentPatrolPoint.position;

            // Debug.Log($"ENEMY {gameObject.name} CAN REACH {setDestination} ");
        }

        // public void ChangePatrolPointSet(int objIdPatrolPointToChange)
        // {
        //     _patrolMovement.ChangePatrolPointSet(objIdPatrolPointToChange);
        // }

        public bool CanGoToPosition(Transform getCurrentPatrolPoint)
        {
            NavMeshPath path = new NavMeshPath();
            _navMeshAgent.CalculatePath(getCurrentPatrolPoint.position, path);
            return path.status == NavMeshPathStatus.PathComplete;
        }

        public void ResetPath()
        {
            _navMeshAgent.ResetPath();
        }

        public void CalculatePath()
        {
            NavMeshPath path = new NavMeshPath();

            do
            {
                _navMeshAgent.CalculatePath(_patrolMovement.GetCurrentPatrolPoint().position, path);
                if (PathInvalid(path))
                    _patrolMovement.SetNewPatrolPoint();
            } while (PathInvalid(path));
        }

        private static bool PathInvalid(NavMeshPath path)
        {
            return path.status == NavMeshPathStatus.PathInvalid || path.status == NavMeshPathStatus.PathPartial;
        }

        public void CalculatePathUpdatingPatrolPoint()
        {
            NavMeshPath path = new NavMeshPath();
            do
            {
                _patrolMovement.SetNewPatrolPoint();
                _navMeshAgent.CalculatePath(_patrolMovement.GetCurrentPatrolPoint().position, path);
            } while (PathInvalid(path));
        }
        
        public Transform GetCurrentCheckpoint()
        {
            return _patrolMovement.GetCurrentPatrolPoint();
        }
    }
}