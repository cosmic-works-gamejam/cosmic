﻿using Presentation.Enemy;
using Presentation.Enemy.IA;
using UnityEngine;

namespace FSM
{
    public class Patrol : State
    {
        private Animator _animator;
        private EnemyBaseFacade _enemyFacade;
        private EnemyMovement _enemyMovement;
        private ObjectDetector _objectDetector;
        private IPatrol _patrol;

        public override void Enter()
        {
            Debug.Log("Entramos en Follow");

            base.Enter();
            _enemyMovement.StartPatrolling();
        }

        public override void Update()
        {
            if (_objectDetector.CanSeeObject() && _enemyMovement.CanGoToPosition(_objectDetector.ObjectToSearch.transform))
            {
                NextState = new Follow(_enemyFacade, _objectDetector.ObjectToSearch.transform);
                new PlayEnemySeePlayerSoundSignal().Execute();
                Stage = EVENT.EXIT;
                return;
            }

            if (EnemyIsNearEnoughToPlayer())
            {
                _enemyFacade.PlaySoundEnemyIsNearEnemy();
            }

            // if (_patrol.IsNearObjectivePatrolPoint())
            // {
            //     _patrol.UpdatePatrolPoint();
            // }

            _enemyMovement.GoToPosition(_patrol.GetCurrentPatrolPoint());
        }

        private bool EnemyIsNearEnoughToPlayer()
        {
            return Vector3.Distance(_enemyFacade.transform.position,
                _objectDetector.ObjectToSearch.transform.position) < _enemyFacade.DistanceToPlayEnemyNearPlayerSound;
        }

        public Patrol(EnemyBaseFacade enemyFacade) : base(enemyFacade)
        {
            Name = STATES.PATROL;
            _animator = enemyFacade.GetAnimator();
            _patrol = enemyFacade.GetPatrol();
            _objectDetector = enemyFacade.GetObjectDetector();
            _enemyMovement = enemyFacade.EnemyMovement;
            _enemyFacade = enemyFacade;
        }
    }
}