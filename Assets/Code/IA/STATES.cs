﻿namespace FSM
{
    public enum STATES
    {
        IDLE,
        WANDER,
        PATROL,
        FOLLOW
    };
}