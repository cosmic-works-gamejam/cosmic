﻿using Presentation.Enemy;
using UnityEngine;

namespace FSM
{
    public class Idle : State
    {
        private Animator _animator;
        private EnemyBaseFacade _enemyFacade;

        public override void Enter()
        {
            base.Enter();
        }


        public override void Update()
        {
            NextState = new Patrol(_enemyFacade);
            Stage = EVENT.EXIT;
        }

        public override void Exit()
        {
            base.Exit();
        }

        public Idle(EnemyBaseFacade enemyFacade) : base(enemyFacade)
        {
            Name = STATES.IDLE;
            _enemyFacade = enemyFacade;
            _animator = enemyFacade.GetAnimator();
        }
    }
}