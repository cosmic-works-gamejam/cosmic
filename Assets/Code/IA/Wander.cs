﻿using Presentation.Enemy;
using UnityEngine;

namespace FSM
{
    public class Wander : State
    {
        private Animator _animator;

        public override void Enter()
        {
            Debug.Log("Entramos en Wander State");
            base.Enter();
        }


        public override void Update()
        {
           
        }

        private bool IsHit()
        {
            return false;
        }

        public override void Exit()
        {
            base.Exit();
        }

        public Wander(EnemyBaseFacade enemyBaseFacade) : base(enemyBaseFacade)
        {
            Name = STATES.WANDER;
            _animator = enemyBaseFacade.GetAnimator();
        }
    }
}