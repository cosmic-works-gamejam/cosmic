﻿using Presentation.Enemy;
using Presentation.Enemy.IA;
using Signals;
using UnityEngine;

namespace FSM
{
    public class Follow : State
    {
        private Animator _animator;
        private EnemyBaseFacade _enemyFacade;
        private EnemyMovement _enemyMovement;
        private ObjectDetector _objectDetector;
        private Transform _objectToSearch;

        public Follow(EnemyBaseFacade enemyFacade, Transform objectDetectorObjectToSearch) : base(enemyFacade)
        {
            _animator = enemyFacade.GetAnimator();
            _objectDetector = enemyFacade.GetObjectDetector();
            _enemyMovement = enemyFacade.EnemyMovement;
            _enemyFacade = enemyFacade;
            _objectToSearch = objectDetectorObjectToSearch;
            Name = STATES.FOLLOW;
        }

        public override void Enter()
        {
            Debug.Log("Entramos en Follow");
            base.Enter();
        }

        public override void Update()
        {
            _enemyMovement.GoToPosition(_objectToSearch);
            if (!_enemyMovement.CanGoToPosition(_objectToSearch))
            {
                NextState = new Patrol(_enemyFacade);
                Stage = EVENT.EXIT;
                return;
            }
            
            if (Vector3.Distance(_enemyFacade.transform.position, _objectToSearch.position) < _objectDetector.DistanceToCatchPlayer)
            {
                _enemyFacade.IsAlive = false;
                NextState = new Patrol(_enemyFacade);

                new PlayerHasBeenCaughtByEnemySignal().Execute();
                Stage = EVENT.EXIT;
            }
        }

        public override void Exit()
        {

            base.Exit();
        }
    }
}