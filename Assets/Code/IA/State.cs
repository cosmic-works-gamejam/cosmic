﻿using Presentation.Enemy;
using UnityEngine;

namespace FSM
{
    public class State
    {
        protected STATES Name;
        protected EVENT Stage;
        protected State NextState;


        public State(EnemyBaseFacade enemyBaseFacade)
        {
        }

        public virtual void Enter()
        {
            Stage = EVENT.UPDATE;
        }

        public virtual void Update()
        {
            Stage = EVENT.UPDATE;
        }

        public virtual void Exit()
        {
            Stage = EVENT.EXIT;
        }

        public State Process()
        {
            if (Stage == EVENT.ENTER) Enter();
            if (Stage == EVENT.UPDATE) Update();
            if (Stage == EVENT.EXIT)
            {
                Exit();
                return NextState;
            }

            return this;
        }
    }
}