﻿using Domain;
using Domain.LoaderSaveGame;
using Signals.EventManager;
using UnityEngine;
using Utils;

public class RetrievePlayerProgression : MonoBehaviour
{
    private IEventManager _eventManager;
    private ILoader _loader;
    
    private void Awake()
    {
        _loader = ServiceLocator.Instance.GetService<ILoader>();
    }
    
    private void OnDestroy()
    {
        ServiceLocator.Instance.UnregisterService<ILoader>();
    }

    public SaveGameCharacterInfo GetSaveGameInfoByCharacterName(CharacterName characterName)
    {
        var loadedSaveGame = _loader.LoadGame().RetrieveInfoByCharacterName(characterName);
        return loadedSaveGame;
    }

    public bool ExistsPreviousSaveGame(CharacterName characterName)
    {
        return GetSaveGameInfoByCharacterName(characterName).LastLevelCompletedSaved > -1;
    }
}