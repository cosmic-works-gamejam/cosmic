﻿using System;

namespace Domain
{
    [Serializable]
    public class SaveGameCharacterInfo
    {
        public int LastLevelCompletedSaved;
        public CharacterName CharacterName;

        public SaveGameCharacterInfo()
        {
            LastLevelCompletedSaved = -1;
            CharacterName = CharacterName.NONE;
        }
    }
}