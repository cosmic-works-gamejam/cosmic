﻿using Domain;
using Domain.LoaderSaveGame;
using Domain.SaverGame;
using Signals.EventManager;
using UnityEngine;
using Utils;

public class UpdatePlayerProgression : MonoBehaviour
{
    private IEventManager _eventManager;
    private ISaver _saver;
    private ILoader _loader;

    void Awake()
    {
        _eventManager = ServiceLocator.Instance.GetService<IEventManager>();
        _saver = ServiceLocator.Instance.GetService<ISaver>();
        _loader = ServiceLocator.Instance.GetService<ILoader>();
    }

    private void Start()
    {
        _eventManager.AddActionToSignal<SavePlayerProgressSignal>(SavePlayerProgress);
    }

    private void OnDestroy()
    {
        _eventManager.RemoveActionFromSignal<SavePlayerProgressSignal>(SavePlayerProgress);
    }

    private void SavePlayerProgress(SavePlayerProgressSignal signal)
    {

        SaveGameData infoToSave = _loader.LoadGame();
        infoToSave.AddInfo(signal.IdSceneToSave, signal.CharacterName);
        _saver.SaveGame(infoToSave);
    }

    public void DeletePlayerProgress()
    {
        _saver.DeleteSaveGame();
    }
}