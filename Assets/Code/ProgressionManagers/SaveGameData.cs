﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Domain
{
    [Serializable]
    public class SaveGameData
    {
        public List<SaveGameCharacterInfo> saveGameCharacterInfo;

        public SaveGameData()
        {
            saveGameCharacterInfo = new List<SaveGameCharacterInfo>();
        }

        public void AddInfo(int lastLevelCompleted, CharacterName characterName)
        {
            bool HasToUpgradeLastCompletedLevel(SaveGameCharacterInfo saveGameCharacterInfo)
            {
                return saveGameCharacterInfo.LastLevelCompletedSaved < lastLevelCompleted;
            }

            if (lastLevelCompleted < 0)
            {
                throw new Exception("Last level completed to save is < 0");
            }

            var lastSaveGame =
                saveGameCharacterInfo.SingleOrDefault(x => x.CharacterName == characterName);

            if (ExistsSaveGame(lastSaveGame))
            {
                if (!HasToUpgradeLastCompletedLevel(lastSaveGame))
                {
                    return;
                }

                saveGameCharacterInfo.Remove(lastSaveGame);
            }

            var infoToSave = new SaveGameCharacterInfo
            {
                CharacterName = characterName, LastLevelCompletedSaved = lastLevelCompleted
            };
            saveGameCharacterInfo.Add(infoToSave);
        }

        public SaveGameCharacterInfo RetrieveInfoByCharacterName(CharacterName characterName)
        {
            var saveGameCharacterInfo = this.saveGameCharacterInfo.SingleOrDefault(x => x.CharacterName == characterName);
            if (saveGameCharacterInfo == null)
            {
                return new SaveGameCharacterInfo() {LastLevelCompletedSaved = -1};
            }

            return saveGameCharacterInfo;
        }

        private bool ExistsSaveGame(SaveGameCharacterInfo lastSaveGame)
        {
            return lastSaveGame != null && lastSaveGame.LastLevelCompletedSaved != -1;
        }
    }
}