﻿public class AudioModel : IAudioModel
{
    private bool _vfxVolume;
    private bool _masterVolume;
    private bool _soundTrackVolume;

    public AudioModel()
    {
        _vfxVolume = true;
        _masterVolume = true;
        _soundTrackVolume = true;
    }
    public bool VFXVolumeStatus
    {
        get => _vfxVolume;
        set => _vfxVolume = value;
    }

    public bool MasterVolumeStatus
    {
        get => _masterVolume;
        set => _masterVolume = value;
    }

    public bool SoundTrackVolumeStatus
    {
        get => _soundTrackVolume;
        set => _soundTrackVolume = value;
    }
}