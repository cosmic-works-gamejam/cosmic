﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MuteAudio : MonoBehaviour
{
    AudioSource audio;
    
    void Start() 
    {
        audio = GameObject.Find("Audio Source").GetComponent<AudioSource>();


        var go = new GameObject { name = "Audio Source" };
        go.AddComponent<AudioSource>();
        DontDestroyOnLoad(go);
    }

    public void AudioToggle()
    {
        if (audio.isPlaying)
        {
            audio.Pause();
        }
        else { audio.Play(); }
    }

}
