﻿using System;
using Signals.EventManager;
using UnityEngine;
using Utils;

public class AudioManagerInitScene : MonoBehaviour
{
    [SerializeField] private AudioSource _audioSourceMenuBackGround, _clickButton;

    private IEventManager _eventManager;

    private void Awake()
    {
        _eventManager = ServiceLocator.Instance.GetService<IEventManager>();
    }

    private void Start()
    {
        _eventManager.AddActionToSignal<PlayClickButtonSignal>(PlayClickButton);

        _audioSourceMenuBackGround.Play();
    }

    private void PlayClickButton(PlayClickButtonSignal obj)
    {
        if (_clickButton.isPlaying) return;
        _clickButton.Play();
    }

    private void OnDestroy()
    {
        _audioSourceMenuBackGround.Stop();
        _eventManager?.RemoveActionFromSignal<PlayClickButtonSignal>(PlayClickButton);
    }
}