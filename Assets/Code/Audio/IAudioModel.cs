﻿public interface IAudioModel
{
    bool VFXVolumeStatus { get; set; }
    bool MasterVolumeStatus { get; set; }
    bool SoundTrackVolumeStatus { get; set; }
}