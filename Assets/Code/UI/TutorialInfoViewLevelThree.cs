﻿using System;
using DG.Tweening;
using Signals.EventManager;
using TMPro;
using UnityEngine;
using Utils;

//Use Cube
public class TutorialInfoViewLevelThree : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI
        infoMoveCubeText,
        _infoHideToCompleteLevelText;


    [SerializeField] private Transform _objectMoveCube,
        _objectHideToCompleteLevelCube,
        _positionToMoveObjectCube,
        _positionToMoveObjectHide;


    [SerializeField] private float _durationToDoDownMovementCube,
        _durationToDoDownMovementHide;


    private Tweener _tweenerWS;
    private ILanguageManager _languageManager;
    private IEventManager _eventManager;

    private void Awake()
    {
        _languageManager = ServiceLocator.Instance.GetService<ILanguageManager>();
        _eventManager = ServiceLocator.Instance.GetService<IEventManager>();
    }

    private void Start()
    {
        _eventManager.AddActionToSignal<ShowMoveCubeInfoTutorialSignal>(delegate { ShowMoveCubeInfo(); });
        SetLanguageTranslations(_languageManager.GetActualLanguageText());
    }

    private void OnDestroy()
    {
        _eventManager.RemoveActionFromSignal<ShowMoveCubeInfoTutorialSignal>(delegate { ShowMoveCubeInfo(); });
    }

    private void SetLanguageTranslations(LanguageText getActualLanguageText)
    {
        infoMoveCubeText.SetText(getActualLanguageText.InfoHowToMoveCubeInGameSceneKey);
        _infoHideToCompleteLevelText.SetText(getActualLanguageText.InfoHideToCompleteLevelInGameSceneKey);
    }


    private void ShowMoveCubeInfo()
    {
        _tweenerWS = UtilsMovements.MoveObjectToPositionWithDuration(_objectMoveCube, _positionToMoveObjectCube,
            _durationToDoDownMovementCube);
        // var positionCubeInfo = _objectMoveCube.transform.position;
        // var valueToMoveDownObjectCubeInfo = positionCubeInfo.y - _positionToMoveObjectCube.position.y;
        // Vector3 destinationPositionObjectMoveCube =
        //     positionCubeInfo - Vector3.up * valueToMoveDownObjectCubeInfo;
        // _tweenerWS =
        //     _objectMoveCube.transform.DOMove(destinationPositionObjectMoveCube, _durationToDoDownMovementCube,
        // false);
        _tweenerWS.onComplete += ShowHideToCompleteLevelInfo;
    }

    private void ShowHideToCompleteLevelInfo()
    {
        _tweenerWS.onComplete -= ShowHideToCompleteLevelInfo;
        UtilsMovements.MoveObjectToPositionWithDuration(_objectHideToCompleteLevelCube, _positionToMoveObjectHide,
            _durationToDoDownMovementHide);
        // var positionCubeHideInfo = _objectHideToCompleteLevelCube.transform.position;
        // var valueToMoveDownObjectHideInfo = positionCubeHideInfo.y - _positionToMoveObjectHide.position.y;
        //
        // Vector3 destinationPositionObjectHideToCompleteLevel =
        //     positionCubeHideInfo - Vector3.up * valueToMoveDownObjectHideInfo;
        // _tweenerWS =
        //     _objectHideToCompleteLevelCube.transform.DOMove(destinationPositionObjectHideToCompleteLevel,
        //         _durationToDoDownMovementHide, false);
    }
}