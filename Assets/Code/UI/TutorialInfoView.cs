﻿using DG.Tweening;
using TMPro;
using UnityEngine;

public class TutorialInfoView1 : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _infoMoveText,
        _infoRotateText,
        _infoJumpText,
        _infoCrouchText,
        _infoMoveCubeText,
        _infoHideToCompleteLevelText,
        _infoSurviveToCompleteLevelText;

    [SerializeField] private Transform _objectWSMove,
        _objectADMove,
        _objectJump,
        _objectCrouch,
        _objectMoveCube,
        _objectHideToCompleteLevelCube,
        _objectSurviveCube,
        _objectAttackEnemyCube;

    [SerializeField] private bool _showInfoMovement;

    [SerializeField] private float _durationToDoDownMovementWS,
        _durationToDoDownMovementAD,
        _durationToDoDownMovementJump,
        _durationToDoDownMovementCrouch,
        _durationToDoDownMovementCube,
        _durationToDoDownHideToCompleteLevel,
        _durationToDoDownSurviveLevel,
        _valueToMoveDownSurviveObject,
        _durationToDoDownAttackEnemyCubeLevel,
        _valueToMoveDownWS,
        _valueToMoveDownAD,
        _valueToMoveDownJumpObject,
        _valueToMoveDownCrouchObject,
        _valueToMoveDownHideToCompleteObject,
        _valueToMoveDownMoveCubeObject,
        _valueToMoveDownAttackEnemyCube;

    private Tweener _tweenerWS;


    private void Start()
    {
        if (!_showInfoMovement) return;
        _objectADMove.gameObject.SetActive(false);
        
    }










    public void SetLanguageTranslations(LanguageText getActualLanguageText)
    {
        
        
        _infoMoveCubeText.SetText(getActualLanguageText.InfoHowToMoveCubeInGameSceneKey);
        _infoHideToCompleteLevelText.SetText(getActualLanguageText.InfoHideToCompleteLevelInGameSceneKey);
        
        
    }


}