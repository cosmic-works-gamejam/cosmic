﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class StoreView : MonoBehaviour
{
    public event Action OnBackButtonPressed;
    [SerializeField] private TextMeshProUGUI _backText, _titleText;

    [SerializeField] private Button _backButton;

    void Start()
    {
        _backButton.onClick.AddListener(() => OnBackButtonPressed.Invoke());
    }

    public void SetLanguageStrings(LanguageText languageText)
    {
        _titleText.SetText(languageText.StoreTitleInitScene);

        _backText.SetText(languageText.BackButtonInitScene);
    }
}