﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ButtonInfo : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _text;
    [SerializeField] private Button _button;
    [SerializeField] private int _id;
    public event Action<ButtonInfo> OnButtonClicked;

    public int ID => _id;

    private void Start()
    {
        _button.onClick.AddListener(() => OnButtonClicked.Invoke(this));
    }

    private void OnDestroy()
    {
        _button.onClick.RemoveListener(() => OnButtonClicked.Invoke(this));
    }

    public void SetText(string text)
    {
        _text.SetText($"{text} {_id+1}");
    }

    public void SetId(int buttonText)
    {
        _id = buttonText;
    }

    public void SetInteractable(bool interactableValue)
    {
        _button.interactable = interactableValue;
    }
}