﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class HistoryCharactersView : MonoBehaviour
{
    public event Action OnBackButtonPressed;
    [SerializeField] private TextMeshProUGUI _backText, _titleText;

    [SerializeField] private Button _backButton;

    // Start is called before the first frame update
    void Start()
    {
        _backButton.onClick.AddListener(() => OnBackButtonPressed.Invoke());
    }

    public void SetLanguageStrings(LanguageText languageText)
    {
        _titleText.SetText(languageText.HistoryTitleInitScene);

        _backText.SetText(languageText.BackButtonInitScene);
    }
}