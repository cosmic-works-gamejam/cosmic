﻿using DG.Tweening;
using Signals.EventManager;
using TMPro;
using UnityEngine;
using Utils;

//Jump & Crouch
public class TutorialInfoViewLevelTwo : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _infoJumpText, _infoCrouchText;

    [SerializeField] private Transform _objectJump,
        _objectCrouch,
        positionToMoveObjectJump,
        _positionToMoveObjectCrouch;


    [SerializeField] private float _durationToDoDownMovementJump, _durationToDoDownMovementCrouch;
    private Tweener _tweenerWS;
    private ILanguageManager _languageManager;
    private IEventManager _eventManager;

    private void Awake()
    {
        _languageManager = ServiceLocator.Instance.GetService<ILanguageManager>();
        _eventManager = ServiceLocator.Instance.GetService<IEventManager>();
    }

    private void Start()
    {
        _eventManager.AddActionToSignal<ShowJumpInfoTutorialSignal>(delegate { ShowJumpInfo(); });
        _eventManager.AddActionToSignal<ShowCrouchInfoTutorialSignal>(delegate { ShowCrouchInfo(); });
        SetLanguageTranslations(_languageManager.GetActualLanguageText());
    }

    private void OnDestroy()
    {
        _eventManager.RemoveActionFromSignal<ShowJumpInfoTutorialSignal>(delegate { ShowJumpInfo(); });
        _eventManager.RemoveActionFromSignal<ShowCrouchInfoTutorialSignal>(delegate { ShowCrouchInfo(); });
    }
    private void SetLanguageTranslations(LanguageText getActualLanguageText)
    {
        _infoJumpText.SetText(getActualLanguageText.InfoHowToJumpInGameSceneKey);
        _infoCrouchText.SetText(getActualLanguageText.InfoHowToCrouchInGameSceneKey);
    }


    private void ShowCrouchInfo()
    {
        UtilsMovements.MoveObjectToPositionWithDuration(_objectCrouch, _positionToMoveObjectCrouch,
            _durationToDoDownMovementCrouch);
        // var positionCrouchInfo = _objectCrouch.transform.position;
        // var valueToMoveDownObjectCrouch = positionCrouchInfo.y - _positionToMoveObjectCrouch.position.y;
        // var destinationPositionObjectCrouch =
        //     positionCrouchInfo - Vector3.up * valueToMoveDownObjectCrouch;
        // _tweenerWS =
        //     _objectCrouch.transform.DOMove(destinationPositionObjectCrouch, _durationToDoDownMovementCrouch, false);
    }

    private void ShowJumpInfo()
    {
        UtilsMovements.MoveObjectToPositionWithDuration(_objectJump, positionToMoveObjectJump,
            _durationToDoDownMovementJump);
        // var positionObjectJump = _objectJump.transform.position;
        // var valueToMoveDownObjectJump = positionObjectJump.y - positionToMoveObjectJump.position.y;
        // var destinationPositionObjectJump =
        //     positionObjectJump - Vector3.up * valueToMoveDownObjectJump;
        // _tweenerWS = _objectJump.transform.DOMove(destinationPositionObjectJump, _durationToDoDownMovementJump, false);
    }
}