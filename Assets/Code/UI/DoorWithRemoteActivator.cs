﻿using Signals.EventManager;
using Utils;

public class DoorWithRemoteActivator : DoorControlBase
{
    private IEventManager _eventManager;

    private void Awake()
    {
        _eventManager = ServiceLocator.Instance.GetService<IEventManager>();
    }

    void Start()
    {
        _portalEffect.gameObject.SetActive(false);

        _portalEffect.OnPlayerHitPortalCollider += OnPlayerEnter;
        _eventManager.AddActionToSignal<ActivateDoorSignal>(delegate { ActivateDoor(); });

        IsActivated = false;
    }

    private void OnDestroy()
    {
        _eventManager.RemoveActionFromSignal<ActivateDoorSignal>(delegate { ActivateDoor(); });
    }
}