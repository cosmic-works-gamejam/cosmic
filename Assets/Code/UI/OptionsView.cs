﻿using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class OptionsView : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _audioVFXText,
        _audioMasterText,
        _audioBackgroundText,
        _audioTitle,
        _languageText,
        _backText,
        _titleText;

    [SerializeField] private Button _backButton;
    [SerializeField] private Toggle _toggleAudioVFX, _toggleAudioMaster, _toggleAudioBackground;
    [SerializeField] private TMP_Dropdown _dropdown;

    public event Action<String> OnLanguageChanged = delegate { };
    public event Action<bool> OnBackgroundVolumeChanged = delegate { };
    public event Action<bool> OnMasterVolumeChanged = delegate { };
    public event Action<bool> OnVFXVolumeChanged = delegate { };

    public event Action OnBackButtonPressed;

    void Start()
    {
        _dropdown.onValueChanged.AddListener(DropdownChanged);
        _toggleAudioVFX.onValueChanged.AddListener(ToggleVFXChanged);
        _toggleAudioMaster.onValueChanged.AddListener(ToggleMasterChanged);
        _toggleAudioBackground.onValueChanged.AddListener(ToggleBackgroundChanged);
        _backButton.onClick.AddListener(() => OnBackButtonPressed.Invoke());
    }

    private void OnDestroy()
    {
        _dropdown.onValueChanged.RemoveListener(DropdownChanged);
        _toggleAudioVFX.onValueChanged.RemoveListener(ToggleVFXChanged);
        _toggleAudioMaster.onValueChanged.RemoveListener(ToggleMasterChanged);
        _toggleAudioBackground.onValueChanged.RemoveListener(ToggleBackgroundChanged);
        _backButton.onClick.RemoveListener(() => OnBackButtonPressed.Invoke());
    }

    private void ToggleMasterChanged(bool status)
    {
        OnMasterVolumeChanged.Invoke(status);
    }

    private void ToggleBackgroundChanged(bool status)
    {
        OnBackgroundVolumeChanged.Invoke(status);
    }

    private void ToggleVFXChanged(bool status)
    {
        OnVFXVolumeChanged.Invoke(status);
    }

    private void DropdownChanged(int arg0)
    {
        OnLanguageChanged.Invoke(_dropdown.options[arg0].text);
    }

    public void SetLanguageStrings(LanguageText languageText)
    {
        _titleText.SetText(languageText.OptionsTitleInitScene);
        _audioVFXText.SetText(languageText.AudioVFXTextInitScene);
        _audioMasterText.SetText(languageText.AudioMasterTextInitScene);
        _audioBackgroundText.SetText(languageText.AudioBackgroundTextInitScene);
        _audioTitle.SetText(languageText.AudioTitleInitScene);
        _languageText.SetText(languageText.LanguageTextInitScene);
        _backText.SetText(languageText.BackButtonInitScene);
    }

    public void SetDropdownValues(List<LanguageName> languageNames, LanguagesKeys currentLanguageKey)
    {
        _dropdown.options.Clear();
        int selectedLanguage = 0;
        for (var index = 0; index < languageNames.Count; index++)
        {
            var languageName = languageNames[index];
            if (languageName.LanguageKey == currentLanguageKey)
            {
                selectedLanguage = index;
            }

            // Debug.Log(languageName.LanguageTitle);
            _dropdown.options.Add(new TMP_Dropdown.OptionData() {text = languageName.LanguageTitle});
        }

        _dropdown.captionText.text = languageNames[selectedLanguage].LanguageTitle;
    }
}