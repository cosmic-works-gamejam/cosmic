﻿using UnityEngine;

public abstract class TutorialBaseInfoView : MonoBehaviour
{
    public abstract void ShowInfoTutorial();
    public abstract void SetLanguageTranslations(LanguageText getActualLanguageText);
}