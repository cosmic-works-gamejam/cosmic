﻿using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class LevelSelectorView : MonoBehaviour
{
    [SerializeField] private List<ButtonInfo> buttonInfo;
    [SerializeField] private Button _buttonBack;
    [SerializeField] private TextMeshProUGUI _buttonBackText, _titleText;
    public event Action<int> OnSelectedLevel = delegate { };
    public event Action OnButtonBackSelected;

    private void Awake()
    {
        for (var index = 0; index < buttonInfo.Count; index++)
        {
            var button = buttonInfo[index];
            button.OnButtonClicked += ButtonClicked;
            button.SetId(index);
            button.SetInteractable(false);
        }
    }

    private void Start()
    {
        _buttonBack.onClick.AddListener(() => OnButtonBackSelected.Invoke());
    }


    private void OnDestroy()
    {
        _buttonBack.onClick.RemoveListener(() => OnButtonBackSelected.Invoke());
    }

    private void ButtonClicked(ButtonInfo button)
    {
        OnSelectedLevel.Invoke(button.ID);
    }

    public void SetLanguageStrings(LanguageText languageText)
    {
        _buttonBackText.SetText(languageText.BackButtonInitScene);
        _titleText.SetText(languageText.LevelSelectorTitleInitScene);

        for (var index = 0; index < buttonInfo.Count; index++)
        {
            var button = buttonInfo[index];
            button.SetText(languageText.levelTextInitSceneKey);
        }
    }


    public void SetEnabledLevelsByCharacterName(int lastLevelCompletedSaved)
    {
        for (var index = 0; index <= lastLevelCompletedSaved; index++)
        {
            var VARIABLE = buttonInfo[index];
            VARIABLE.SetInteractable(true);
        }

        if (lastLevelCompletedSaved + 1 >= buttonInfo.Count) return;
        buttonInfo[lastLevelCompletedSaved + 1].SetInteractable(true);
    }
}