﻿using Signals;
using Signals.EventManager;
using TMPro;
using UnityEngine;
using Utils;
using Utils.Input;

public class CanvasPresenterInGame : MonoBehaviour
{
    [SerializeField] private GameOverView _gameOverMenuView;
    [SerializeField] private PauseMenuView _pauseMenuView;
    // [SerializeField] private TutorialBaseInfoView tutorialBaseInfoView;
    [SerializeField] private TimeControl _timeManager;
    [SerializeField] private TextMeshProUGUI remainTimeText;

    private ILanguageManager _languageManager;
    private ReadInputPlayer _readInputPlayer;
    private SceneChanger _sceneChanger;
    private IEventManager _eventManager;

    void Awake()
    {
        _languageManager = ServiceLocator.Instance.GetService<ILanguageManager>();
        _eventManager = ServiceLocator.Instance.GetService<IEventManager>();

        _eventManager.AddActionToSignal<PlayerHasBeenCaughtByEnemySignal>(PlayerHasLost);
        _readInputPlayer = ServiceLocator.Instance.GetService<ReadInputPlayer>();
        _sceneChanger = ServiceLocator.Instance.GetService<SceneChanger>();
    }

    private void Start()
    {

        _gameOverMenuView.gameObject.SetActive(false);
        _pauseMenuView.gameObject.SetActive(false);
        HideCursor();
        ResumeTimeScale();
        ShowTime();

        // _tutorialInfoView.SetLanguageTranslations(_languageManager.GetActualLanguageText());
        _gameOverMenuView.SetLanguageTranslations(_languageManager.GetActualLanguageText());
        _pauseMenuView.SetLanguageTranslations(_languageManager.GetActualLanguageText());

        _readInputPlayer.OnDebugEscPressed += PauseGame;
        _gameOverMenuView.OnExitLevel += GoToInitialScene;
        _gameOverMenuView.OnRestartGame += RestartLevel;
        _pauseMenuView.OnExitLevel += GoToInitialScene;
        _pauseMenuView.OnRestartGame += RestartLevel;
        _pauseMenuView.OnResumeGame += ResumeLevel;
        _timeManager.OnTimeHasPast += () => new ActivateDoorSignal().Execute();
        
    }
    
    private void OnDestroy()
    {
        _eventManager.RemoveActionFromSignal<PlayerHasBeenCaughtByEnemySignal>(PlayerHasLost);
        _readInputPlayer.OnDebugEscPressed -= PauseGame;
        _gameOverMenuView.OnExitLevel -= GoToInitialScene;
        _gameOverMenuView.OnRestartGame -= RestartLevel;
        _pauseMenuView.OnExitLevel -= GoToInitialScene;
        _pauseMenuView.OnRestartGame -= RestartLevel;
        _pauseMenuView.OnResumeGame -= ResumeLevel;
        _timeManager.OnTimeHasPast -= () => new ActivateDoorSignal().Execute();
    }

    private void ResumeTimeScale()
    {
        Time.timeScale = 1;
    }

    private void PauseTimeScale()
    {
        Time.timeScale = 0;
    }

    private void ResumeLevel()
    {
        HideCursor();
        new PlayClickButtonSignal().Execute();
        _pauseMenuView.gameObject.SetActive(false);
        ResumeTimeScale();
    }

    private static void HideCursor()
    {
        Cursor.visible = false;
    }

    private void RestartLevel()
    {
        new PlayClickButtonSignal().Execute();

        new PlayRestartLevelSoundSignal().Execute();
        _sceneChanger.RestartScene();
    }

    private void GoToInitialScene()
    {
        new PlayClickButtonSignal().Execute();

        _sceneChanger.GoToInitScene();
    }

    private void PauseGame()
    {
        _pauseMenuView.gameObject.SetActive(true);
        PauseTimeScale();
        ShowCursor();
    }

    private static void ShowCursor()
    {
        Cursor.visible = true;
    }

    private void Update()
    {
        if (!_timeManager.TimeFinished)
        {
            ShowTime();
        }
    }

    private void ShowTime()
    {
        remainTimeText.text = _timeManager.GetTime().ToString("0.0");
    }

    private void PlayerHasLost(PlayerHasBeenCaughtByEnemySignal obj)
    {
        new PlayGameOverSoundSignal().Execute();
        Debug.Log("DEAD");
        PauseTimeScale();
        ShowGameOverMenu();
        ShowCursor();
    }

    private void ShowGameOverMenu()
    {
        _gameOverMenuView.gameObject.SetActive(true);
    }
}