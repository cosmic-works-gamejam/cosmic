﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CreditsView : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI _backText, _titleText, _programmersTitle, _gameDesignerTitle, _artistVFXTitle, _compositorsTitle, _artist3DTitle, artist2DTitle;
    [SerializeField] private Button _backButton;
    public event Action OnBackButtonPressed;

    void Start()
    {
        _backButton.onClick.AddListener(() => OnBackButtonPressed.Invoke());
    }

    public void SetLanguageStrings(LanguageText languageText)
    {
        _titleText.SetText(languageText.TitleCreditsSceneCreditsKey);
        _programmersTitle.SetText(languageText.ProgrammersTitleCreditsKey);
        _artistVFXTitle.SetText(languageText.ArtistVFXTitleCreditsKey);
        _compositorsTitle.SetText(languageText.CompositorsVFXTitleCreditsKey);
        _artist3DTitle.SetText(languageText.Artist3DTitleCreditsKey);
        artist2DTitle.SetText(languageText.Artist2DTitleCreditsKey);
        _gameDesignerTitle.SetText(languageText.GameDesignerTitleCreditsKey);
        _backText.SetText(languageText.ExitButtonSceneCreditsKey);
    }
}