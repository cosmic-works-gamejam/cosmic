﻿using System;
using DG.Tweening;
using Signals.EventManager;
using TMPro;
using UnityEngine;
using Utils;

//Survive the time
public class TutorialInfoViewLevelFour : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _infoSurviveToCompleteLevelText;


    [SerializeField] private Transform _objectSurviveCube, _positionToMoveSurviveCube;


    [SerializeField] private float _durationToDoDownSurviveLevel;

    private Tweener _tweenerWS;
    private ILanguageManager _languageManager;
    private IEventManager _eventManager;

    private void Awake()
    {
        _languageManager = ServiceLocator.Instance.GetService<ILanguageManager>();
        _eventManager = ServiceLocator.Instance.GetService<IEventManager>();
    }

    private void Start()
    {
        _eventManager.AddActionToSignal<ShowSurviveInfoTutorialSignal>(delegate
        {
            UtilsMovements.MoveObjectToPositionWithDuration(_objectSurviveCube, _positionToMoveSurviveCube,
                _durationToDoDownSurviveLevel);
        });
        SetLanguageTranslations(_languageManager.GetActualLanguageText());
    }

    private void OnDestroy()
    {
        _eventManager.RemoveActionFromSignal<ShowSurviveInfoTutorialSignal>(delegate
        {
            UtilsMovements.MoveObjectToPositionWithDuration(_objectSurviveCube, _positionToMoveSurviveCube,
                _durationToDoDownSurviveLevel);
        });
    }

    private void SetLanguageTranslations(LanguageText getActualLanguageText)
    {
        _infoSurviveToCompleteLevelText.SetText(getActualLanguageText.InfoSurviveToCompleteLevelInGameSceneKey);
    }
}

//Survive the time