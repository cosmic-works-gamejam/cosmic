﻿using App.PlayerModelInfo;
using Domain;
using TMPro;
using UnityEngine;
using Utils;

public class CanvasInitScenePresenter : MonoBehaviour
{
    [SerializeField] private InitSceneView _initSceneView;
    [SerializeField] private OptionsView _optionsView;
    [SerializeField] private CreditsView _creditsView;
    [SerializeField] private StoreView _storeView;
    [SerializeField] private HistoryCharactersView _historyCharacters;
    [SerializeField] private CharacterSelectorView _characterSelectorView;
    [SerializeField] private LevelSelectorView _levelSelectorView;
    [SerializeField] private TextMeshProUGUI _versionText, _versionNameText;
    [SerializeField] private AudioManagerBase _audioManagerBase;

    private IPlayerModel _playerModel;
    private IAudioModel _audioModel;
    private RetrievePlayerProgression _retrievePlayerProgression;
    private UpdatePlayerProgression _updatePlayerProgression;
    private ILanguageManager _languageManager;
    private SceneChanger _sceneChanger;

    void Awake()
    {
        _retrievePlayerProgression = ServiceLocator.Instance.GetService<RetrievePlayerProgression>();
        _languageManager = ServiceLocator.Instance.GetService<ILanguageManager>();
        _sceneChanger = ServiceLocator.Instance.GetService<SceneChanger>();
        _updatePlayerProgression = ServiceLocator.Instance.GetService<UpdatePlayerProgression>();
        _playerModel = ServiceLocator.Instance.GetModel<IPlayerModel>();
        _audioModel = ServiceLocator.Instance.GetModel<IAudioModel>();
    }

    void Start()
    {
        SetVersionInfo();

        ShowInitView();
        _optionsView.gameObject.SetActive(false);
        _creditsView.gameObject.SetActive(false);
        _storeView.gameObject.SetActive(false);
        _characterSelectorView.gameObject.SetActive(false);
        _levelSelectorView.gameObject.SetActive(false);
        _historyCharacters.gameObject.SetActive(false);

        _optionsView.OnBackButtonPressed += HandleOptionsCharactersBackButton;
        _optionsView.OnLanguageChanged += HandleLanguageChanged;
        _optionsView.OnBackgroundVolumeChanged += BackgroundVolumeChanged;
        _optionsView.OnMasterVolumeChanged += MasterVolumeChanged;
        _optionsView.OnVFXVolumeChanged += VFXVolumeChanged;

        _creditsView.OnBackButtonPressed += HandleCreditsBackButton;
        _storeView.OnBackButtonPressed += HandleHistoryCharactersBackButton;
        _historyCharacters.OnBackButtonPressed += HandleHistoryCharactersBackButton;

        _initSceneView.SetLanguageStrings(_languageManager.GetActualLanguageText());
        _initSceneView.OnOptionsButtonPressed += ShowOptions;
        _initSceneView.OnCreditsButtonPressed += ShowCredits;
        _initSceneView.OnPlayButtonPressed += InitPlayMode;
        _initSceneView.OnExitButtonPressed += ExitGame;
        _initSceneView.OnStoreButtonPressed += ShowStore;
        _initSceneView.OnShowCharactersHistoryButtonPressed += ShowCharactersHistory;

        _characterSelectorView.OnButtonBackSelected += HandleCharacterSelectorBackButton;
        _characterSelectorView.OnCharacterOneSelected += HandleCharacterOneSelectedButton;
        _characterSelectorView.OnCharacterTwoSelected += HandleCharacterTwoSelectedButton;
        _characterSelectorView.OnCharacterThreeSelected += HandleCharacterThreeSelectedButton;

        _levelSelectorView.OnSelectedLevel += SelectedLevelToLoad;
        _levelSelectorView.OnButtonBackSelected += HandleLevelSelectorBackButton;
    }

    private void MasterVolumeChanged(bool status)
    {
        _audioManagerBase.UpdateMasterVolume(status);
    }

    private void BackgroundVolumeChanged(bool status)
    {
        _audioManagerBase.UpdateBackgroundVolume(status);
    }

    private void HandleLanguageChanged(string obj)
    {
        LanguagesKeys languagesKeysToChange = _languageManager.GetLanguageKeyByName(obj);
        _languageManager.SetActualLanguageText(languagesKeysToChange);
        _optionsView.SetLanguageStrings(_languageManager.GetActualLanguageText());
        SetVersionInfo();
    }

    private void VFXVolumeChanged(bool status)
    {
        _audioManagerBase.UpdateVFXVolume(status);
    }


    private void SelectedLevelToLoad(int idLevel)
    {
        _sceneChanger.LoadCurrentLevelFromInitScene(idLevel, _playerModel.CharacterNameSelected);
    }

    private void SetVersionInfo()
    {
        _versionText.SetText($"v{Application.version}");
        _versionNameText.SetText(_languageManager.GetActualLanguageText().VersionTextInitSceneKey);
    }

    private void HandleLevelSelectorBackButton()
    {
        new PlayClickButtonSignal().Execute();

        _characterSelectorView.SetLanguageStrings(_languageManager.GetActualLanguageText());
        _characterSelectorView.gameObject.SetActive(true);
        _levelSelectorView.gameObject.SetActive(false);
    }

    private void HandleCharacterThreeSelectedButton(CharacterName characterName)
    {
        new PlayClickButtonSignal().Execute();

        _playerModel.CharacterNameSelected = characterName;
        CheckLevelToLoadByCharacter(characterName);
    }

    private void HandleCharacterTwoSelectedButton(CharacterName characterName)
    {
        new PlayClickButtonSignal().Execute();

        _playerModel.CharacterNameSelected = characterName;
        CheckLevelToLoadByCharacter(characterName);
    }

    private void HandleCharacterOneSelectedButton(CharacterName characterName)
    {
        new PlayClickButtonSignal().Execute();

        _playerModel.CharacterNameSelected = characterName;
        CheckLevelToLoadByCharacter(characterName);
    }

    private void CheckLevelToLoadByCharacter(CharacterName characterName)
    {
        _characterSelectorView.gameObject.SetActive(false);

        if (ExistsPreviousSaveGame(characterName))
        {
            _levelSelectorView.gameObject.SetActive(true);
            var saveGameCharacterInfo =
                _retrievePlayerProgression.GetSaveGameInfoByCharacterName(characterName);
            _levelSelectorView.SetEnabledLevelsByCharacterName(saveGameCharacterInfo.LastLevelCompletedSaved);
            _levelSelectorView.SetLanguageStrings(_languageManager.GetActualLanguageText());
            return;
        }

        _sceneChanger.GoToFirstLevel(characterName);
    }

    private void HandleCharacterSelectorBackButton()
    {
        _characterSelectorView.gameObject.SetActive(false);
        ShowInitView();
    }


    private void HandleOptionsCharactersBackButton()
    {
        _optionsView.gameObject.SetActive(false);
        ShowInitView();
    }

    private void HandleCreditsBackButton()
    {
        _creditsView.gameObject.SetActive(false);
        ShowInitView();
    }

    private void HandleHistoryCharactersBackButton()
    {
        _historyCharacters.gameObject.SetActive(false);
        ShowInitView();
    }

    private void ShowStore()
    {
        new PlayClickButtonSignal().Execute();

        _storeView.gameObject.SetActive(true);
        _storeView.SetLanguageStrings(_languageManager.GetActualLanguageText());
    }


    private void ShowInitView()
    {
        new PlayClickButtonSignal().Execute();

        _initSceneView.SetLanguageStrings(_languageManager.GetActualLanguageText());

        _initSceneView.gameObject.SetActive(true);
    }

    private void HideInitView()
    {
        _initSceneView.gameObject.SetActive(false);
    }

    private void ShowCharactersHistory()
    {
        new PlayClickButtonSignal().Execute();

        HideInitView();
        _historyCharacters.gameObject.SetActive(true);
        _historyCharacters.SetLanguageStrings(_languageManager.GetActualLanguageText());
    }
    
    private void ExitGame()
    {
        new PlayClickButtonSignal().Execute();

        Application.Quit();
    }

    private void InitPlayMode()
    {
        new PlayClickButtonSignal().Execute();
        HideInitView();

        HandleCharacterOneSelectedButton(CharacterName.Kyle);
        // _characterSelectorView.gameObject.SetActive(true);
        // _characterSelectorView.SetLanguageStrings(_languageManager.GetActualLanguageText());
    }

    private void ShowCredits()
    {
        new PlayClickButtonSignal().Execute();

        HideInitView();
        _creditsView.gameObject.SetActive(true);
        _creditsView.SetLanguageStrings(_languageManager.GetActualLanguageText());
    }

    private void ShowOptions()
    {
        new PlayClickButtonSignal().Execute();

        HideInitView();
        _optionsView.gameObject.SetActive(true);
        _optionsView.SetLanguageStrings(_languageManager.GetActualLanguageText());
        _optionsView.SetDropdownValues(_languageManager.GetLanguageNames(), _languageManager.GetActualLanguageKey());
    }

    private bool ExistsPreviousSaveGame(CharacterName characterName)
    {
        return _retrievePlayerProgression.ExistsPreviousSaveGame(characterName);
    }
}