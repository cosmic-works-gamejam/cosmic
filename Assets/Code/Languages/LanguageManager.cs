﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[Serializable]
public class LanguageName
{
    public LanguagesKeys LanguageKey;
    public string LanguageTitle;
}

public class LanguageManager : MonoBehaviour, ILanguageManager
{
    [SerializeField] private List<LanguageInfo> _languageInfos;
    [SerializeField] private List<LanguageName> _languageNames;
    private LanguageText _actualLanguageText;
    private LanguagesKeys _actualLanguageKey;

    public LanguageManager()
    {
    }

    public LanguageText GetActualLanguageText()
    {
        return _actualLanguageText;
    }

    public LanguagesKeys GetActualLanguageKey()
    {
        return _actualLanguageKey;
    }

    // public string GetLanguageNameByKey(LanguagesKeys languagesKeyToChange)
    // {
    //     return _languageNames.Single(x => x.LanguageKey == languagesKeyToChange).LanguageTitle;
    // }

    public LanguagesKeys GetLanguageKeyByName(string languagesNameToChange)
    {
        return _languageNames.Single(x => x.LanguageTitle == languagesNameToChange).LanguageKey;
    }

    public void SetActualLanguageText(LanguagesKeys languagesKeyToChange)
    {
        _actualLanguageText = _languageInfos.Single(x => x.LanguagesKey == languagesKeyToChange).LanguageText;
        _actualLanguageKey = languagesKeyToChange;
    }

    public List<LanguageName> GetLanguageNames()
    {
        return _languageNames;
    }
}