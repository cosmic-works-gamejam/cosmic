﻿using System.Collections.Generic;

public interface ILanguageManager
{
    LanguageText GetActualLanguageText();
    LanguagesKeys GetActualLanguageKey();
    // string GetLanguageNameByKey(LanguagesKeys languagesKeyToChange);
    LanguagesKeys GetLanguageKeyByName(string languagesNameToChange);
    List<LanguageName> GetLanguageNames();
    void SetActualLanguageText(LanguagesKeys languagesKeyToChange);

}