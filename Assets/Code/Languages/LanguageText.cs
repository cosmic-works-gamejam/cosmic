﻿using UnityEngine;

public abstract class LanguageText : ScriptableObject
{
    public string ExitButtonInitScene;
    public string PlayButtonInitScene;
    public string ShowCharactersHistoryButtonInitScene;
    public string OptionsButtonInitScene;
    public string CreditsButtonInitScene;
    public string InfoHowToMoveInGameSceneKey;
    public string InfoHowToRotateInGameSceneKey;
    public string InfoHowToJumpInGameSceneKey;
    public string InfoHowToCrouchInGameSceneKey;
    public string InfoHowToMoveCubeInGameSceneKey;
    public string InfoHideToCompleteLevelInGameSceneKey;
    public string InfoSurviveToCompleteLevelInGameSceneKey;
    public string InfoHowToAttackEnemyInGameSceneKey;
    public string ExitButtonMenuInGameKey;
    public string RestartButtonMenuInGameKey;
    public string ResumeButtonPauseMenuInGameKey;
    public string TitleGameOverMenuInGameKey;
    public string TitlePauseMenuInGameKey;
    public string Artist2DTitleCreditsKey;
    public string Artist3DTitleCreditsKey;
    public string CompositorsVFXTitleCreditsKey;
    public string ArtistVFXTitleCreditsKey;
    public string ProgrammersTitleCreditsKey;
    public string TitleCreditsSceneCreditsKey;
    public string GameDesignerTitleCreditsKey;
    public string ExitButtonSceneCreditsKey;
    public string StoreButtonInitScene;
    public string AudioTextInitScene;
    public string LanguageTextInitScene;
    public string BackButtonInitScene;
    public string OptionsTitleInitScene;
    public string HistoryTitleInitScene;
    public string StoreTitleInitScene;
    public string VersionTextInitSceneKey;
    public string levelTextInitSceneKey;
    public string SelectCharacterToPlayInitScene;
    public string LevelSelectorTitleInitScene;
    public string CharactersTitleInitScene;
    public string AudioBackgroundTextInitScene;
    public string AudioTitleInitScene;
    public string AudioMasterTextInitScene;
    public string AudioVFXTextInitScene;
}