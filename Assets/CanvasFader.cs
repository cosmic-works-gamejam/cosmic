using System;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class CanvasFader : MonoBehaviour
{
    public event Action OnFadeCompleted;
    public event Action OnUnfadeCompleted;

    [SerializeField] private Image _fader;


    public void ActivateFader()
    {
        Debug.Log("Activamos Fade");
        var fader = _fader.DOFade(1, 1f);
        fader.Play();
        fader.onComplete += () => OnFadeCompleted.Invoke();
    }


    public void DeactivateFader()
    {
        Debug.Log("Desactivamos Fade");
        var fader = _fader.DOFade(0, 2f);
        fader.Play();
        fader.onComplete += () => OnUnfadeCompleted.Invoke();
    }
}