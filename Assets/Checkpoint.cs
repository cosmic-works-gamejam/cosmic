using Presentation.Enemy;
using UnityEngine;

public class Checkpoint : MonoBehaviour
{
    // Start is called before the first frame update
    private void OnTriggerEnter(Collider other)
    {
        var enemy = other.GetComponent<EnemyBaseFacade>();
        if (!enemy) return;
        if (enemy.EnemyMovement.GetCurrentCheckpoint() != transform) return;
        enemy.EnemyMovement.CalculatePathUpdatingPatrolPoint();
    }
}
