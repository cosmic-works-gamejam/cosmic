using UnityEngine;
using UnityEngine.SceneManagement;
using Utils;

public class LoadingSceneController : MonoBehaviour
{
    private ISceneModel _sSceneModel;
    private float _progress;

    private AsyncOperation operationNextScene;
    [SerializeField] private LoadingSceneFader _canvasFader;

    void Start()
    {
        _sSceneModel = ServiceLocator.Instance.GetModel<ISceneModel>();

        operationNextScene = SceneManager.LoadSceneAsync(_sSceneModel.NextScene, LoadSceneMode.Additive);
        SceneManager.SetActiveScene(SceneManager.GetSceneByName(_sSceneModel.NextScene));
        operationNextScene.completed += SceneLoaded;
    }

    private void SceneLoaded(AsyncOperation obj)
    {
        operationNextScene.completed -= SceneLoaded;
        _canvasFader.OnUnfadeCompleted += ChangeToNewScene;
        _canvasFader.DeactivateUI();
        _canvasFader.DeactivateFader();
    }

    private void ChangeToNewScene()
    {
        SceneManager.UnloadSceneAsync(_sSceneModel.LoadingScene);
        _canvasFader.OnUnfadeCompleted -= ChangeToNewScene;
        operationNextScene.allowSceneActivation = true;
    }
    
    private void Update()
    {
        if (operationNextScene.isDone) return;
        _progress = Mathf.Clamp01(operationNextScene.progress / 0.9f) * 100f;
        Debug.Log($"Progress {_progress}");
        _canvasFader.SetText(_progress.ToString());
    }
}