using System;
using System.Collections;
using System.Collections.Generic;
using Presentation.Enemy;
using Signals.EventManager;
using UnityEngine;
using Utils;

public class EnemyController : MonoBehaviour
{
    protected IEventManager _eventManager;
    [SerializeField] private List<EnemyBaseFacade> _enemies;
    private void Awake()
    {
        _eventManager = ServiceLocator.Instance.GetService<IEventManager>();
    }

    // Start is called before the first frame update
    void Start()
    {
        _eventManager.AddActionToSignal<ResetEnemiesPathSignal>(ResetPath);
        _eventManager.AddActionToSignal<ResetEnemyPathSignal>(ResetPathForEnemy);
    }

    private void ResetPathForEnemy(ResetEnemyPathSignal obj)
    {
        obj.EnemyBaseFacade.ResetPath();
        obj.EnemyBaseFacade.CalculatePathUpdatingPatrolPoint();
    }

    private void ResetPath(ResetEnemiesPathSignal obj)
    {
        foreach (var enemy in _enemies)
        {
            enemy.ResetPath();
            enemy.CalculatePath();
        }
    }
}
